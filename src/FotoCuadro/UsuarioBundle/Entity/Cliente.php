<?php

namespace FotoCuadro\UsuarioBundle\Entity;
use FotoCuadro\UsuarioBundle\Lib\Model\Desactivable;

use Symfony\Component\Validator\Constraints\DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use FotoCuadro\UsuarioBundle\Lib\Util\Util;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\Mapping\Index;

/**
 * Entidad Cliente de FotoCuadro
 * @ORM\Table(name="cliente")
 * @ORM\Entity(repositoryClass="FotoCuadro\UsuarioBundle\Entity\ClienteRepository")
 */
class Cliente implements Desactivable{

	/**
	 * Identificador del cliente
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	/**
	 * Nombre del cliente
	 * @ORM\Column(type="string", length=60, nullable=false)
	 */
	protected $nombre;
	/**
	 * Apellidos del cliente
	 * @ORM\Column(type="string", length=120, nullable=true)
	 */
	protected $apellidos;
	/**
	 * Email del cliente
	 * @ORM\Column(type="string", length=60, nullable=false)
	 */
	protected $email;
	/**
	 * Teléfono del cliente
	 * @ORM\Column(type="string", length=20, nullable=true)
	 */
	protected $telefono;
	/**
	 * Indicador de si el cliente está activo o no
	 * @ORM\Column(type="boolean", nullable=false)
	 */
	protected $activo;
	/**
	 * Fecha de creación del cliente
	 * @ORM\Column(type="datetime", nullable=false)
	 */
	protected $creado;
	/**
	 * Lista con los pedidos que ha hecho el usuario
	 * @ORM\OneToMany(targetEntity="FotoCuadro\PedidoBundle\Entity\Pedido", mappedBy="usuario")
	 */
	private $pedidos;
	
	/**
	 * Contructor de la clase.
	 * Crea los valores por defecto para el cliente
	 */
	public function __construct() {
		$this->activo = true;
		$this->pedidos = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Método mágico toString para la clase, imprime el nombre del cliente
	 */
	public function __toString() {
		return $this->nombre;
		
	}
	/**
	 * Getter para el ID del cliente
	 * @return int 
	 */
	public function getId() {
		return $this->id;
	}	
	/**
	 * Getter para el nombre del cliente
	 * @return string
	 */
	public function getNombre() {
		return $this->nombre;
	}
	/**
	 * Setter para el nombre del cliente
	 * @param string $nombre
	 */
	public function setNombre($nombre) {
		$this->nombre = $nombre;
	}
	/**
	 * Getter para los apellidos del cliente
	 * @return string
	 */
	public function getApellidos() {
		return $this->apellidos;
	}
	/**
	 * Setter para los apellidos del cliente
	 * @param string $apellidos
	 */
	public function setApellidos($apellidos) {
		$this->apellidos = $apellidos;
	}
	/**
	 * Getter para el email
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}
	/**
	 * Setter para el email
	 * @param string $email
	 */
	public function setEmail($email) {
		$this->email = $email;
	}
	/**
	 * Getter para el telefono
	 * @return string
	 */
	public function getTelefono() {
		return $this->telefono;
	}
	/**
	 * Setter para el telefono
	 * @param string $telefono
	 */
	public function setTelefono($telefono) {
		$this->telefono = $telefono;
	}
	/**
	 * Getter para el indicador de si el cliente está activo o no
	 * @return boolean
	 */
	public function isActivo() {
		return $this->activo;
	}
	/**
	 * Getter para la fecha de creación del cliente
	 * @return \DateTime
	 */
	public function getCreado() {
		return $this->creado;
	}
	/**
	 * Setter para la fecha de creación del cliente
	 * @param \DateTime $creado
	 */
	public function setCreado(\DateTime $creado) {
		$this->creado = $creado;
	}
	/**
	 * Método para desactivar el cliente
	 */
	public function desactivar() {
		$this->activo = false;
	}
	/**
	 * Método para activar el cliente
	 */
	public function activar() {
		$this->activo = true;
	}
	/**
	 * Getter para los pedidos del usuario
	 * @return array
	 */
	public function getPedidos() {
		return $this->pedidos;
	}
	/**
	 * Setter para los pedidos del usuario
	 * @param array
	 */
	public function setPedidos($pedidos) {
		$this->pedidos = $pedidos;
	}
}
