<?php
namespace FotoCuadro\UsuarioBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * EntityRepository para el Usuario 
 * @author Conqueingredientes
 */
class UsuarioRepository extends EntityRepository {
	/**
	 * Metodo que busca todos los usuarios que puede ver un usuario
	 * @param Usuario $usuario
	 * @return Mixed
	 */
	public function findAllUsuarios($usuario = null)
	{
		$em = $this->getEntityManager();
		
		$dql = "SELECT u FROM UsuarioBundle:Usuario u";
		
		if ($usuario) {
			if ($usuario->getRol() == Usuario::ROL_AGENTE) 
				$dql = "SELECT u FROM UsuarioBundle:Usuario u 
						WHERE u.creado_por = " . $usuario->getId();
														
			else if ($usuario->getRol() == Usuario::ROL_FOTOGRAFO) 
				return null;			
		}
		
		$consulta = $em->createQuery($dql);
		
		return $consulta->getResult();
	}
	
	/**
	 * Método que busca un usuario en la base de datos en función de su id
	 * @param integer $id
	 * @return Usuario
	 */
	public function findUsuarioById($id) {
		$em = $this->getEntityManager();
		
		$dql = "SELECT u FROM UsuarioBundle:Usuario u
				WHERE u.id = :id";			
				
		$consulta = $em->createQuery($dql);
		$consulta->setParameter('id', $id);
		$consulta->setMaxResults(1);
		
		return $consulta->getOneOrNullResult();
	}
}
