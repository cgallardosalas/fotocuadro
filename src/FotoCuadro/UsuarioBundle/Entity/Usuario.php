<?php

namespace FotoCuadro\UsuarioBundle\Entity;
use FotoCuadro\UsuarioBundle\Lib\Model\Desactivable;

use FotoCuadro\ClubBundle\Entity\UsuarioClub;

use Symfony\Component\Validator\Constraints\DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use FotoCuadro\UsuarioBundle\Lib\Util\Util;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\Mapping\Index;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Entidad Usuario de FotoCuadro
 * @ORM\Table(name="usuario") 
 * @ORM\Entity(repositoryClass="FotoCuadro\UsuarioBundle\Entity\UsuarioRepository")
 */
class Usuario implements UserInterface, Desactivable {

	const ROL_ADMIN = 0;
	const ROL_AGENTE = 1;
	const ROL_FOTOGRAFO = 2;
	/**
	 * Identificador del cliente
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	/**
	 * Nombre del cliente
	 * @ORM\Column(type="string", length=60, nullable=false)
	 */
	protected $nombre;
	/**
	 * Apellidos del cliente
	 * @ORM\Column(type="string", length=120, nullable=true)
	 */
	protected $apellidos;
	/**
	 * Email del cliente
	 * @ORM\Column(type="string", length=60, nullable=false)
	 */
	protected $email;
	/**
	 * Teléfono del cliente
	 * @ORM\Column(type="string", length=20, nullable=true)
	 */
	protected $telefono;
	/**
	 * Indicador de si el cliente está activo o no
	 * @ORM\Column(type="boolean", nullable=false)
	 */
	protected $activo;
	/**
	 * Fecha de creación del cliente
	 * @ORM\Column(type="datetime", nullable=false)
	 */
	protected $creado;
	/**
	 * Usuario que ha creado al usuario
	 * @ORM\ManyToOne(targetEntity="FotoCuadro\UsuarioBundle\Entity\Usuario")
	 * @ORM\JoinColumn(name="creado_por", referencedColumnName="id", onDelete="SET NULL", onUpdate="CASCADE")
	 */
	protected $creado_por;
	/**
	 * Password del usuario
	 * @ORM\Column(type="string", length=250)
	 */
	protected $password;
	/**
	 * imagen del usuario
	 * @ORM\Column(type="string", length=60, nullable=true)
	 */
	protected $imagen;	
	/**
	 * Fecha de último acceso del usuario
	 * @ORM\Column(type="datetime", nullable=false)
	 */
	protected $ultimo_acceso;
	/**
	 * Lista con los clubes a los que tiene acceso el usuario
	 * @ORM\OneToMany(targetEntity="FotoCuadro\ClubBundle\Entity\UsuarioClub", mappedBy="usuario")
	 */
	protected $clubes;
	/**
	 * Salt para la generación de la contraseña
	 * @ORM\Column(name="salt", type="string", length=250)
	 */
	protected $salt;
	/**
	 * Rol del usuario
	 * @ORM\Column(name="rol", type="integer", nullable=false)
	 */
	protected $rol;
	/**
	 * Lista con los pedidos que ha hecho el usuario
	 * @ORM\OneToMany(targetEntity="FotoCuadro\PedidoBundle\Entity\Pedido", mappedBy="usuario")
	 */
	protected $pedidos;

	/**
	 * Contructor de la clase.
	 */
	public function __construct() {		
		$this->activo = true;
		$this->salt = "mysalttoday";
		$this->clubes = new \Doctrine\Common\Collections\ArrayCollection();
		$this->pedidos = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Método que devuelve los roles para un usuario
	 */
	public static function getTiposRoles() {
		return array(self::ROL_ADMIN => 'Administrador',
				self::ROL_AGENTE => 'Agente',
				self::ROL_FOTOGRAFO => 'Fotógrafo');
	}
	
	/**
	 * Método mágico toString para la clase, imprime el nombre del usuario
	 */
	public function __toString() {
		return $this->nombre;
	
	}
	/**
	 * Getter para el ID del usuario
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}
	/**
	 * Indicador de si es admin
	 * @return boolean
	 */
	public function isAdmin() {
		return $this->rol == self::ROL_ADMIN;
	}
	/**
	 * Setter para el indicador del rol del usuario
	 * @param integer $rol
	 */
	public function setRol($rol) {
		$this->rol = $rol;
	}
	/**
	 * Getter para el indicador del rol del usuario
	 * @param integer $rol
	 */
	public function getRol() {
		return $this->rol;
	}
	
	/**
	 * Getter para el nombre del usuario
	 * @return string
	 */
	public function getNombre() {
		return $this->nombre;
	}
	/**
	 * Setter para el nombre del usuario
	 * @param string $nombre
	 */
	public function setNombre($nombre) {
		$this->nombre = $nombre;
	}
	
	/**
	 * Getter para los apellidos del usuario
	 * @return string
	 */
	public function getApellidos() {
		return $this->apellidos;
	}
	/**
	 * Setter para los apellidos del usuario
	 * @param string $apellidos
	 */
	public function setApellidos($apellidos) {
		$this->apellidos = $apellidos;
	}
	/**
	 * Getter para el email
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}
	/**
	 * Setter para el email
	 * @param string $email
	 */
	public function setEmail($email) {
		$this->email = $email;
	}
	/**
	 * Getter para el telefono
	 * @return string
	 */
	public function getTelefono() {
		return $this->telefono;
	}
	/**
	 * Setter para el telefono
	 * @param string $telefono
	 */
	public function setTelefono($telefono) {
		$this->telefono = $telefono;
	}
	/**
	 * Getter para el indicador de si el usuario está activo o no
	 * @return boolean
	 */
	public function isActivo() {
		return $this->activo;
	}
	/**
	 * Setter para activo 
	 * @param boolean $flag
	 */
	public function setActivo ($flag) {
		$this->activo = $flag;
	}
	/**
	 * Getter para la fecha de creación del usuario
	 * @return \DateTime
	 */
	public function getCreado() {
		return $this->creado;
	}
	/**
	 * Setter para la fecha de creación del usuario
	 * @param \DateTime $creado
	 */
	public function setCreado(\DateTime $creado) {
		$this->creado = $creado;
	}

	/**
	 * Getter del creador del usuario
	 * @return Usuario
	 */
	public function getCreadoPor() {
		return $this->creado_por;
	}
	/**
	 * Setter del creador del usuario
	 * @param Usuario $usuario
	 */
	public function setCreadoPor($usuario) {
		$this->creado_por = $usuario;
	}
	/**
	 * Getter de la password del usuario
	 * @return string
	 */
	public function getPassword() {
		return $this->password;
	}
	/**
	 * Setter de la password del usuario
	 * @param string $password
	 */
	public function setPassword($password) {
		$this->password = $password;
	}
	/**
	 * Getter de la imagen del usuario
	 * @return string
	 */
	public function getImagen() {
		return $this->imagen;
	}
	/**
	 * Setter de la imagen del usuario
	 * @param string $imagen
	 */
	public function setImagen($imagen) {
		$this->imagen = $imagen;
	}
	/**
	 * Getter del último acceso del usuario
	 * @return \DateTime
	 */
	public function getUltimoAcceso() {
		return $this->ultimo_acceso;
	}
	/**
	 * Setter del último acceso del usuario
	 * @param \DateTime $acceso
	 */
	public function setUltimoAcceso(\DateTime $acceso) {
		$this->ultimo_acceso = $acceso;
	}
	/**
	 * Getter para los clubes del usuario
	 * @return array
	 */
	public function getClubes() {
		return $this->clubes;
	}
	/**
	 * Setter para los clubes del usuario
	 * @param array
	 */
	public function setClubes($clubes) {
		$this->clubes = $clubes;
	}
	
	/**
	 * Getter para los pedidos del usuario
	 * @return array
	 */
	public function getPedidos() {
		return $this->pedidos;
	}
	/**
	 * Setter para los pedidos del usuario
	 * @param array
	 */
	public function setPedidos($pedidos) {
		$this->pedidos = $pedidos;
	}
	
	/**
	 * Método que añade un club al que puede acceder el usuario
	 * @param UsuarioClub $club
	 */
	public function addClub(UsuarioClub $club) {
		$this->clubes->add($club);
	}
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Security\Core\User.UserInterface::getSalt()
	 */
	public function getSalt() {
		return $this->salt;		
	}
	/**
	 * Setter para el salt
	 * @param string $salt
	 */
	public function setSalt($salt) {
		$this->salt = $salt;
	}	
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Security\Core\User.UserInterface::getUsername()
	 */
	public function getUsername() {
		return $this->getNombre();
	}
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Security\Core\User.UserInterface::eraseCredentials()
	 */
	public function eraseCredentials() {

	}
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Security\Core\User.UserInterface::equals()
	 */
	public function equals(UserInterface $user) {
		return $this->getEmail() == $user->getEmail();
	}
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Security\Core\User.UserInterface::getRoles()
	 */
	public function getRoles() {
		if ($this->getRol() == self::ROL_ADMIN)
			return array('ROLE_ADMIN');
		else if ($this->getRol() == self::ROL_AGENTE)
			return array('ROLE_AGENTE');
		else
			return array('ROLE_FOTO');
	}
	/**
	 * Método para desactivar el usuario
	 */
	public function desactivar() {
		$this->activo = false;
	}
	/**
	 * Método para activar el usuario
	 */
	public function activar() {
		$this->activo = true;
	}
}
