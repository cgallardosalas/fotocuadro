<?php
namespace FotoCuadro\UsuarioBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * EntityRepository para el Cliente 
 * @author Conqueingredientes
 */
class ClienteRepository extends EntityRepository {
	
	/**
	 * Método que busca un cliente en la base de datos en función de su id
	 * @param integer $id
	 * @return Cliente
	 */
	public function findClienteById($id) {
		$em = $this->getEntityManager();
	
		$dql = "SELECT c FROM UsuarioBundle:Cliente c
		WHERE c.id = :id";
	
		$consulta = $em->createQuery($dql);
		$consulta->setParameter('id', $id);
		$consulta->setMaxResults(1);
	
		return $consulta->getOneOrNullResult();
	}
}
