<?php

namespace FotoCuadro\UsuarioBundle\Lib\Util;
use Symfony\Component\Yaml\Yaml;

/**
 * Clase para conectar mediante PDO con la Base de Datos
 * @author Carlos Gallardo
 */
class DBConnection {
	
	private static $hostname;
	private static $database;
	private static $username;
	private static $password;
	
	/**
	 * Method to connect with the database
	 * @return \PDO
	 */
	public static function connect () {
		try {
			// get the configuration
			self::getConfig();
						
			$dsn = "mysql:host=".self::$hostname.";dbname=".self::$database;
			$db_conn = new \PDO($dsn, self::$username, self::$password);
			return $db_conn;
		} catch (PDOException $e) {
			die ("Could not connect to database");
		} catch (Exception $a) {
			die ($a->getMessage());
		}
	} 
	
	/**
	 * Method to get the configuration to connect to the database
	 */
	private static function getConfig () {
		
		$config_file = __DIR__.'/../../../../../app/config/config.yml';
		
		if (file_exists($config_file)) {
			$config = Yaml::parse($config_file);
			
			$file = __DIR__.'/../../../../../app/config/'.$config['imports'][0]['resource'];
			
			if (file_exists($file)) {
					
				$loader = parse_ini_file($file);
					
				if ($loader) {
					self::$hostname = $loader['database_host'];
					self::$database = $loader['database_name'];
					self::$username = $loader['database_user'];
					self::$password = $loader['database_password'];
				}
			}
		}
			
	}
}
