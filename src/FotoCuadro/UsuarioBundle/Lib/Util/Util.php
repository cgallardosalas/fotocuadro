<?php

namespace FotoCuadro\UsuarioBundle\Lib\Util;

/**
 * Clase con métodos útiles
 * @author Carlos Gallardo
 */
class Util {			
	/**
	 * Dump an object with the <pre> label
	 * @param Object $object
	 */
	static public function preVardump ($object) {
		echo "<pre>";
		var_dump($object);
		echo "</pre>";				
	} 
}
