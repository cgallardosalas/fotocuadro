<?php

namespace FotoCuadro\UsuarioBundle\Lib;

/**
 * Clase imagen para mover imagenes, redimensionar, etc
 * @author Carlos Gallardo
 */
abstract class Imagen {

	const IMAGEN_USUARIO = 1;
	const IMAGEN_GALERIA = 2;	
	const IMAGEN_CLUB = 3;
	
	/**
	 * Método que guarda una imagen en la ruta asociada segun el tipo de imagen que se trate
	 * @param file $archivo
	 * @param integer $tipo
	 * @param integer $galeria
	 */
	static public function guardarImagen ($archivo, $tipo = 0, $galeria = null) {
		// creo que faltaria adaptarlo para la galeria
		
		$ruta_usuario = __DIR__ . "/../../../../web/fotos/usuario/";
		$ruta_galeria = __DIR__ . "/../../../../web/fotos/galeria/";
		$ruta_club = __DIR__ . "/../../../../web/fotos/club/";
		
		if ($archivo) {		

			if ($tipo == self::IMAGEN_USUARIO)
				$directory = $ruta_usuario;
			else if ($tipo == self::IMAGEN_GALERIA)
				$directory = $ruta_galeria . strval($galeria) . "/";
			else if ($tipo == self::IMAGEN_CLUB)
				$directory = $ruta_club;
			else 
				return false;
			
			$archivo->move($directory, strtolower($archivo->getClientOriginalName()));						
		
			return file_exists($directory . strtolower($archivo->getClientOriginalName()));		
		}				
		return false;
	} 
	
	/**
	 * Método que cambia el tamaño de una imagen recortándola
	 * @param unknown_type $pathTemp
	 * @param unknown_type $pathFoto
	 * @param unknown_type $anchoMax
	 * @param unknown_type $altoMax
	 */
	public function resizeImagen($pathTemp, $pathFoto, $anchoMax, $altoMax) {
	
		$typeImg = pathinfo($pathTemp, PATHINFO_EXTENSION);
	
		list($widthLogo, $heightLogo) = getimagesize($pathTemp);
	
		// Foto demasiado apaisada
		if($widthLogo/$heightLogo > $anchoMax/$altoMax) {
	
			$anchoOriginal = $heightLogo * $anchoMax / $altoMax;
			$altoOriginal = $heightLogo;
	
		} else {
			// foto demasiado vertical
			$anchoOriginal = $widthLogo;
			$altoOriginal = $widthLogo * $altoMax / $anchoMax;
	
		}
	
		$thumb = imagecreatetruecolor($anchoMax, $altoMax);
	
		if($typeImg == 'png') {
			$source = imagecreatefrompng($pathTemp);
		} else if(($typeImg == 'jpeg') || ($typeImg == 'jpg')) {
			$source = imagecreatefromjpeg($pathTemp);
		} else if($typeImg == 'gif') {
			$source = imagecreatefromgif($pathTemp);
		} else return;
	
		imagecopyresized($thumb, $source, 0, 0, 0, 0, $anchoMax, $altoMax, $anchoOriginal, $altoOriginal);
		imageJPEG($thumb,$pathFoto);
		imageDestroy($thumb);
	}
	
	
}
