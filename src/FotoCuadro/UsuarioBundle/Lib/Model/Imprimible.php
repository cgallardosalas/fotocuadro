<?php

namespace FotoCuadro\UsuarioBundle\Lib\Model;

interface Imprimible {		
	
	/**
	 * Método que imprime un PDF 
	 */
	public function imprimir();
		
}