<?php

namespace FotoCuadro\UsuarioBundle\Lib\Model;

interface Desactivable {		
	
	/**
	 * Método que desactiva una entidad 
	 */
	public function desactivar();
	
	/**
	 * Método que activa una entidad
	 */
	public function activar();
		
}