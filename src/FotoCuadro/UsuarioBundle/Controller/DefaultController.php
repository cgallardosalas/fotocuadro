<?php

namespace FotoCuadro\UsuarioBundle\Controller;

use FotoCuadro\UsuarioBundle\Lib\Imagen;

use FotoCuadro\ClubBundle\Entity\UsuarioClub;

use FotoCuadro\UsuarioBundle\Form\UsuarioType;
use FotoCuadro\ClubBundle\Form\ClubType;

use FotoCuadro\UsuarioBundle\Entity\Usuario;
use FotoCuadro\ClubBundle\Entity\Club;

use FotoCuadro\UsuarioBundle\Lib\Util\Util;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;


class DefaultController extends Controller
{
    
    public function panelAction()    
    {   
    	$em = $this->getDoctrine()->getEntityManager();
    	// cojo los datos del usuario
    	$user = $this->get('security.context')->getToken()->getUser(); 
    	$informe = $em->getRepository('PedidoBundle:Pedido')->infoPedidos();  	    	
    	// si el usuario no es un fotografo
    	if ($user->getRol() != Usuario::ROL_FOTOGRAFO) {
    		$form = $this->createForm(new ClubType(), new Club());
    		// muestro el formulario
    		return $this->render('UsuarioBundle:Default:panel.html.twig',
    				array(
    						'form' => $form->createView(),    						
    						'rol' => $user->getRol(),
    						'informe' => $informe,
    				));
    	} else {
    		return $this->render('UsuarioBundle:Default:panel.html.twig',
    				array(
    						'form' => null,    						
    						'rol' => $user->getRol(),
    						'informe' => $informe,
    				));
    	} 	    	    			    	            
    }
    
    public function createAction()
    {    	  
    	// creo el usuario
    	$entity = new Usuario();    	
    	// obtengo la informacion de la request
    	$request = $this->getRequest();    	
    	$user_clubs = $request->request->get("usuario");    	
    	$user_clubs = (isset($user_clubs["clubes"])) ? $user_clubs["clubes"] : array();
    	
    	// creo el fomulario y uno los resultados
    	$form = $this->createForm(new UsuarioType(null, Usuario::getTiposRoles()), $entity);
    	$form->bindRequest($request);
    	// si es valido inserto en la base de datos
    	if ($form->isValid()) {
    		    		
    		$em = $this->getDoctrine()->getEntityManager();   
    		$clubes = $em->getRepository('ClubBundle:Club')->findAllClubes();
    		// establezco la fecha de creación
    		$entity->setCreado(new \DateTime());
    		$entity->setUltimoAcceso(new \DateTime());
    		// vacio los clubes ya que no funciona auto
    		$entity->setClubes(new \Doctrine\Common\Collections\ArrayCollection()); 
    		// asigno el usuario padre
    		$user = $this->get('security.context')->getToken()->getUser();
    		$entity->setCreadoPor($user);
    		// meto la contraseña    		    	
    		$factory = $this->get('security.encoder_factory');			
			$encoder = $factory->getEncoder($entity);
			$password = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
			$entity->setPassword($password);    								    		
    		// guardo la foto
    		$files = $request->files->get('usuario', null);    	
    		if (isset($files['imagen'])) {
    			$fileUpload = $files['imagen'];
    			$entity->setImagen(strtolower($fileUpload->getClientOriginalName()));
    			Imagen::guardarImagen($fileUpload, Imagen::IMAGEN_USUARIO);
    		}    		    		
    		// persisto el usuario 
    		$em->persist($entity);
    		$em->flush();
    		// por cada rol que se ha metido por usuario
			foreach ($user_clubs as $club) {
				$idClub = intval($club['club']);
				if (array_key_exists($idClub, $clubes)) {
					$uc = new UsuarioClub();
					$uc->setClub($clubes[$idClub]);					
					$uc->setUsuario($entity);
					$em->persist($uc);					
				} 
			}
			$em->flush();    		    		   
    	}    	
    	    	
    	return $this->redirect($this->generateUrl('menu_usuario'));
    }
    
    
    public function indexAction()
    {
    	// cojo los datos del usuario
    	$user = $this->get('security.context')->getToken()->getUser();
    	// obtener listado de usuarios OJO mostrar solo los que puede editar
    	$em = $this->getDoctrine()->getEntityManager();
    	$lista = $em->getRepository('UsuarioBundle:Usuario')->findAllUsuarios($user);
    	$roles = Usuario::getTiposRoles();
    	// si el usuario no es un fotografo
    	if ($user->getRol() != Usuario::ROL_FOTOGRAFO) {
    		$form = $this->crearFormAltaUsuario();    		
    		// muestro el formulario
    		return $this->render('UsuarioBundle:Default:index.html.twig',
    				array(
    						'form' => $form->createView(),
    						'lista' => $lista,
    						'roles' => $roles,
    				));
    	} else {
    		return $this->render('UsuarioBundle:Default:index.html.twig',
    				array(
    						'form' => null,
    						'lista' => $lista,
    						'roles' => $roles,
    				));
    	}
    }
    
    
    public function editAction($id)
    {
    	$em = $this->getDoctrine()->getEntityManager();
    	//recupero la informacion del usuario a editar
    	$entity = $em->getRepository('UsuarioBundle:Usuario')->findUsuarioById($id);
    	     	
    	if (!$entity) {
    		throw $this->createNotFoundException('No se ha podido encontrar el usuario.');
    	}    	
    	// creo el formulario de edición
    	$editForm = $this->crearFormAltaUsuario($entity);
    	
    	// cojo los datos del usuario
    	$user = $this->get('security.context')->getToken()->getUser();
    	// si el usuario no es un fotografo
    	if ($user->getRol() != Usuario::ROL_FOTOGRAFO) {
    		$form = $this->crearFormAltaUsuario();    	
    		// muestro el formulario
    		return $this->render('UsuarioBundle:Default:edit.html.twig',
    				array(
    						'form' => $form->createView(),
    						'entity' => $entity,    						
    				));
    	} else {
    		return $this->render('UsuarioBundle:Default:edit.html.twig',
    				array(
    						'form' => null,  
    						'entity' => null,
    				));
    	}
    	
    }
    
    /**
     * Método que crea el formulario de la creación del usuario
     */
    private function crearFormAltaUsuario ($usuario = null) {
    	
    	// cojo las opciones para el formulario de creación del usuario    	
    	$roles = Usuario::getTiposRoles();
    	$user = $this->get('security.context')->getToken()->getUser();
    	$em = $this->getDoctrine()->getEntityManager();
    	if (!$user->isAdmin()) {    		
    		// elimino el rol del administrador y el agente
    		unset($roles[Usuario::ROL_ADMIN]);  
    		unset($roles[Usuario::ROL_AGENTE]);
    		// cojo los clubes del usuario, en los que tiene permiso
    		$clubes = $em->getRepository('ClubBundle:Club')->findClubesByUser($user->getId());
    	} else     		
    		$clubes = $em->getRepository('ClubBundle:Club')->findAllClubes();
    	
    	// si es un alta de usuario
    	if (!$usuario) {
    		// creo el nuevo usuario
    		$usuario = new Usuario();
    		// creo el formulario de creación del usuario
    		$form = $this->createForm(new UsuarioType($clubes, $roles), $usuario);
    	} else {
    		$form = $this->createForm(new UsuarioType($clubes, $roles), $usuario);
    	}
    		    	
    	return $form;    	
    }
    
	/**
     * Acción para mostrar el formulario del login y para loguear al usuario
     * @return \Symfony\Component\HttpFoundation\Response
     */
	public function loginAction()
    {        	
        $request = $this->getRequest();
        $session = $request->getSession();                       
 
        // meto la contraseña
        /*$factory = $this->get('security.encoder_factory');
        $entity = new Usuario();
        $entity->setPassword("1234");        
        $encoder = $factory->getEncoder($entity);
        $password = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
        $entity->setPassword($password);
        
        echo $entity->getPassword();
        die();*/
        
        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
        }        
        
        return $this->render('UsuarioBundle:Default:login.html.twig', array(
            // last username entered by the user
            'last_username' => $session->get(SecurityContext::LAST_USERNAME),
            'error'         => $error,
        ));
    }
	
	/**
     * @Route("/logout", name="logout_usuario")
     */
    public function logoutAction()
    {
        // The security layer will intercept this request
    }
}
