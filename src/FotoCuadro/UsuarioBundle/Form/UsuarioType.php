<?php

namespace FotoCuadro\UsuarioBundle\Form;

use FotoCuadro\UsuarioBundle\Entity\Usuario;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use FotoCuadro\UsuarioBundle\Form\RolClubType;

/**
 * Clase que utilizamos para crear el formulario de creación de un usuario
 */
class UsuarioType extends AbstractType
{
	/**
	 * Opciones para el listado de clubs
	 * @var array
	 */
	private $opc_club;
	/**
	 * Opciones para el listado de roles
	 * @var array
	 */
	private $opc_rol;
	
	/**
	 * Constructor de la clase 
	 */
	public function __construct($clubes = null, $roles = null) {		
		$this->opc_club = $clubes;
		$this->opc_rol = $roles;
	}
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Form.AbstractType::buildForm()
	 */
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
        	->add('nombre', 'text', array('required' => true))
        	->add('apellidos', 'text', array('required' => false))
        	->add('rol', 'choice', array('required' => true,
        			'choices' => $this->opc_rol,
        			'expanded' => false,
        			'multiple' => false))
        	->add('email', 'email', array('required' => true))
        	->add('telefono', 'text', array('label' => 'Teléfono', 'required' => false))                                                          
            ->add('activo', 'checkbox', array('required' => false))                                   
            ->add('password', 'password', array('required' => true))           
            ->add('imagen', 'file', array('required' => false, "attr" => array(
            		"accept" => "image/*")))
            ->add('clubes', 'collection', array(
            		'type' => new UsuarioClubType($this->opc_club),
            		'allow_add' => true,
            		'by_reference' => false,            		
            ))
        ;
    }
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Form.FormTypeInterface::getName()
	 */
    public function getName()
    {
        return 'usuario';
    }
}
