<?php

namespace FotoCuadro\UsuarioBundle\Form;

use FotoCuadro\ClubBundle\Entity\ClubRepository;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

/**
 * Clase que utilizamos para crear el formulario de creación de un usuario
 */
class UsuarioClubType extends AbstractType
{
	/**
	 * Opciones para el listado de clubs
	 * @var array
	 */
	private $opc_club;
	
	/**
	 * Constructor de la clase
	 */
	public function __construct($clubes = null) {		
		$this->opc_club = ($clubes) ? $clubes : array('EMPTY' => 'No hay clubes disponibles');
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Form.AbstractType::buildForm()
	 */
    public function buildForm(FormBuilder $builder, array $options)
    {
    	// ver las opciones que tengo    	
        $builder        	
        	->add('club', 'choice', array(
        			'required' => true,
        			'choices' => $this->opc_club,
        			'expanded' => false,
        			'multiple' => false))  			        		
        	
        	->add('usuario', 'entity', array('class' => 'FotoCuadro\UsuarioBundle\Entity\Usuario', 'property'=>'id', 'multiple'  => false, 'attr'=> array('style'=>'display:none') ))        	        	                                                                     
        ;
                
    }
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Form.FormTypeInterface::getName()
	 */
    public function getName()
    {
        return 'usuarioclub';
    }
        

}
