<?php

namespace FotoCuadro\UsuarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

/**
 * Clase que utilizamos para crear el formulario de creación de un cliente
 */
class ClienteType extends AbstractType
{
	/**
	 * ç(non-PHPdoc)
	 * @see Symfony\Component\Form.AbstractType::buildForm()
	 */
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
        	->add('nombre', 'text', array('required' => true))
        	->add('apellidos', 'text', array('required' => false))
        	->add('email', 'email', array('required' => true))
        	->add('telefono', 'text', array('label' => 'Teléfono', 'required' => false))                                                                                                            
        ;
    }
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Form.FormTypeInterface::getName()
	 */
    public function getName()
    {
        return 'cliente';
    }
}
