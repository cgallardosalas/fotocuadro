<?php


namespace FotoCuadro\PedidoBundle\Controller;

use FotoCuadro\UsuarioBundle\Form\ClienteType;

use FotoCuadro\UsuarioBundle\Entity\Cliente;

use FotoCuadro\PedidoBundle\Entity\Producto;

use FotoCuadro\UsuarioBundle\Lib\Util\Util;

use FotoCuadro\PedidoBundle\Form\PedidoType;

use FotoCuadro\PedidoBundle\Entity\Pedido;
use FotoCuadro\UsuarioBundle\Entity\Usuario;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{
	/**
	 * Acción para el index de pedidos con el listado de pedidos y el boton
	 */
    public function indexAction()
    {    	    	
    	// cojo los datos del usuario
    	$user = $this->get('security.context')->getToken()->getUser();
    	// obtener listado de pedidos
    	$em = $this->getDoctrine()->getEntityManager();
    	$pedidos = $em->getRepository('PedidoBundle:Pedido')->findPedidosByUser($user);    	
    	
    	return $this->render('PedidoBundle:Default:index.html.twig',
    			array(    					
    					'pedidos' => $pedidos, 
    					'estados' => Pedido::getEstados(),   					
    			));    	
    }
    
    /**
     * Acción para editar un pedido
     * @param integer $id
     */
    public function editAction($id)
    {
    	$em = $this->getDoctrine()->getEntityManager();
    	//recupero la informacion del pedido a editar
    	$entity = $em->getRepository('PedidoBundle:Pedido')->findPedidoById($id);
    	if (!$entity) {
    		throw $this->createNotFoundException('No se ha podido encontrar el pedido.');
    	}
    	// creo el formulario de edición
    	//$editForm = $this->createForm(new PedidoType(), $entity);
    	// muestro el formulario
    	return $this->render('PedidoBundle:Default:edit.html.twig',
    			array(
    					'estados' => Pedido::getEstados(),
    					'entity' => $entity,
    			));
    
    }
    
    /**
     * Acción para el primer paso del pedido, mostrar la selección de clubes
     */
    public function newClubesAction()
    {    	
    	// cojo los datos del usuario
    	$user = $this->get('security.context')->getToken()->getUser();
    	// obtener listado de equipos
    	$em = $this->getDoctrine()->getEntityManager();
    	if ($user->getRol() == Usuario::ROL_ADMIN)
    		$lista = $em->getRepository('ClubBundle:Club')->findAllClubes();
    	else
    		$lista = $em->getRepository('ClubBundle:Club')->findClubesByUser($user->getId());
    	
    	// creo el pedido    			  
    	$pedido = array(
    			'id' => null,
    			'fecha' => new \DateTime(),
    			'usuario' => $user->getId(),
    			'cliente' => null,
    			'importe' => 0.00,
    			'pagado' => 0.00,
    			'estado' => Pedido::PEDIDO_PENDIENTE,
    			'incidencias' => '',
    			'pagos' => array(),
    			'productos' => array());    	    
    	
    	// y lo guardo en sesión
    	$sesion = $this->getRequest()->getSession();
    	$sesion->set('pedido', $pedido);
    	
    	return $this->render('PedidoBundle:Default:new.html.twig',
    				array(
    						'lista' => $lista,
    						'paso' => 1));
    }
    
    /**
     * Acción para el segundo paso del pedido, mostrar la lista de categorias del club
     * @param integer $idClub Id del club
     */
    public function newCategoriasAction($idClub)
    {    	
    	// obtener listado de equipos
    	$em = $this->getDoctrine()->getEntityManager();
    	$club = $em->getRepository('ClubBundle:Club')->findClubById($idClub);
    	
    	if (!$club)
    		throw $this->createNotFoundException('No se ha podido encontrar el club.');    	    	            	    	    	    
    	 
    	return $this->render('PedidoBundle:Default:new.html.twig',
    			array(    					
    					'paso' => 2,
    					'club' => $club));
    }
    
    /**
     * Acción para el tercer paso del pedido, mostrar la lista de galerias del club
     * @param integer $categoria Id de de la categoria
     */
    public function newGaleriasAction($categoria)
    {
    	// obtener listado de equipos
    	$em = $this->getDoctrine()->getEntityManager();
    	//$galerias = $em->getRepository('GaleriaBundle:Galeria')->findGaleriasByIdCategoria($categoria);
    	
    	$categoria = $em->getRepository('ClubBundle:Club')->findCategoriaById($categoria);
    	 
    	if (!$categoria)
    		throw $this->createNotFoundException('No se ha podido encontrar la categoria.');    	     
    
    	return $this->render('PedidoBundle:Default:new.html.twig',
    			array(
    					'categoria' => $categoria,
    					'paso' => 3));
    }
    
    /**
     * Acción para el cuarto paso del pedido, mostrar la lista de fotos de la galeria
     * @param integer $galeria Id de de la galeria
     */
    public function newFotosAction($galeria)
    {    	
    	$em = $this->getDoctrine()->getEntityManager();
		//recupero la informacion de la galeria a mostrar
		$galeria = $em->getRepository('GaleriaBundle:Galeria')->findGaleriaById($galeria);
		if (!$galeria) {
			throw $this->createNotFoundException('No se ha podido encontrar la galería.');
		}		
    
    	return $this->render('PedidoBundle:Default:new.html.twig',
    			array(
    					'galeria' => $galeria,
    					'paso' => 4));
    }
    
    /**
     * Acción para la seleccion de las fotos del pedido     
     */
    public function seleccionFotosAction()
    {
    	// obtengo la informacion de la request
    	$request = $this->getRequest();
    	// cojo el id de la galeria
    	$idGaleria = $request->request->get("galeria_id");
    	     	
    	// busco los ids de las fotos seleccionadas
    	if ($request->request->get("fotos_pedido")) {    		
    		$seleccion = $request->request->get("fotos_pedido");
    		// y lo guardo en sesión
    		$sesion = $this->getRequest()->getSession();
    		$sesion->set('fotos', $seleccion);
    		$sesion->set('galeria', $idGaleria);    		
    		// muestro la primera foto 
    		$em = $this->getDoctrine()->getEntityManager();
    		$galeria = $em->getRepository('GaleriaBundle:Galeria')->findGaleriaById($idGaleria);
    		// inicio el indice
    		$indice = 0;    		
			// recupero la foto 
    		$foto = $em->getRepository('GaleriaBundle:Galeria')->findFotoById(intval($seleccion[0]));
    		// cargo los tipos de productos
    		$tipos = $em->getRepository('PedidoBundle:Pedido')->findAllTiposProducto();
    		return $this->render('PedidoBundle:Default:new.html.twig',
    				array(
    						'foto' => $foto,
    						'categoria' => $galeria->getCategoria(),
    						'galeria' => $galeria,
    						'tipos' => $tipos,
    						'indice' => (count($seleccion) - 1 == $indice) ? null : $indice,
    						'paso' => 5));
    		
    	} else {    	    		    		    
    		// redirijo a la pagina que estaba
    		$this->get('session')->setFlash('notice', 'Sebe seleccionar al menos una foto para el pedido');
    		return $this->redirect($this->generateUrl('pedido_new_fotos', array ('galeria' => $idGaleria)));
    	}      
    }
    
    /**
     * Acción para ir guardando el pedido de cada una de las fotos seleccionada
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function guardarProductoAction () {        	
    	$em = $this->getDoctrine()->getEntityManager();
    	$sesion = $this->getRequest()->getSession();
    	// obtengo la informacion de la request
    	$request = $this->getRequest();
    	// cojo la información de la galeria
    	$idGaleria = intval($sesion->get('galeria'));    	    	
    	$galeria = $em->getRepository('GaleriaBundle:Galeria')->findGaleriaById($idGaleria);
    	// cojo la información del listado de las fotos
    	$seleccion = $sesion->get('fotos');
    	// cojo el indice de la foto que se muestra
    	$indice = 1 + intval($request->request->get("indice_foto"));
    	// si es la primera vez, lo guardo en sesion
    	$foto = $em->getRepository('GaleriaBundle:Galeria')->findFotoById(intval($seleccion[$indice]));
    	// cargo los tipos de productos
    	$tipos = $em->getRepository('PedidoBundle:Pedido')->findAllTiposProducto();    	
    	// añado al pedido
    	$pedido = $sesion->get('pedido');
    	$cantidades = $request->request->get('tipo_cantidad', array());    	    	
    	$fotoId = $request->request->get('fotoId', null);
    	foreach ($cantidades as $key => $valor) {    		    		
    		if (is_numeric($valor)) {    			
    			$producto = array(
    							'pedido' => null,
    							'foto' => $fotoId,
    							'tipo_producto' => $tipos[$key]->getId(),
    							'cantidad' => intval($valor));
    			
    			$pedido['productos'][] = $producto;     			
    		}
    	}  
    	// guardo el pedido en la sesion
    	$sesion->set('pedido', $pedido);    	
    	
    	return $this->render('PedidoBundle:Default:new.html.twig',
    			array(
    					'foto' =>$foto,
    					'categoria' => $galeria->getCategoria(),
    					'galeria' => $galeria,
    					'tipos' => $tipos,
    					'indice' => (count($seleccion) - 1 == $indice) ? null : $indice,
    					'paso' => 5));    
    }
    /**
     * Accion para confirmar el pedido
     */
    public function confirmarAction()
    {
    	$em = $this->getDoctrine()->getEntityManager();
    	$sesion = $this->getRequest()->getSession();
    	// obtengo la informacion de la request
    	$request = $this->getRequest();    	
    	// cargo los tipos de productos
    	$tipos = $em->getRepository('PedidoBundle:Pedido')->findAllTiposProducto();
    	// añado al pedido
    	$pedido = $sesion->get('pedido');
    	$cantidades = $request->request->get('tipo_cantidad', array());
    	$fotoId = $request->request->get('fotoId', null);
    	foreach ($cantidades as $key => $valor) {
    		if (is_numeric($valor)) {
    			$producto = array(
    					'pedido' => null,
    					'foto' => $fotoId,
    					'tipo_producto' => $tipos[$key]->getId(),
    					'cantidad' => intval($valor));
    			 
    			$pedido['productos'][] = $producto;
    		}
    	}

    	$total = 0.00;
    	// cargo los tipos por ID
    	$tipos = $em->getRepository('PedidoBundle:Pedido')->findAllTiposProductoWithKeyId();
    	// calculo el total a pagar    	
    	foreach ($pedido['productos'] as $producto) 
    		// id del producto
    		$total += $producto['cantidad'] * $tipos[$producto['tipo_producto']]->getPrecio(); 
    	
    	$pedido['importe'] = $total;
    	// guardo el pedido en la sesion
    	$sesion->set('pedido', $pedido);
    	$cliente = new Cliente();    	
    	$form = $this->createForm(new ClienteType(), $cliente);
    	return $this->render('PedidoBundle:Default:confirmar.html.twig',
    			array(
    					'form' => $form->createView(),
    					'total' => $total,
    				 ));    	
    }
    
    /**
     * Método para crear el pedido
     */
    public function crearAction () {
    	
    	$em = $this->getDoctrine()->getEntityManager();
    	
    	$entity = new Cliente();
    	// obtengo la informacion de la request
    	$request = $this->getRequest();  
    	// dinero a cuenta
    	$a_cuenta = $request->request->get("acuenta");    	    	       	
    	// creo el fomulario y uno los resultados
    	$form = $this->createForm(new ClienteType(), $entity);
    	$form->bindRequest($request);
    	// establezco la fecha de creación
    	$entity->setCreado(new \DateTime());
    	// persisto el club
    	$em->persist($entity);
    	$em->flush();
    	// guardo el nombre el cliente
    	$sesion = $this->getRequest()->getSession();
    	$pedido = $sesion->get('pedido');
    	$pedido['cliente'] = $entity->getId();
    	$pedido['pagado'] = ($a_cuenta == '') ? 0.00 : floatval($a_cuenta);
    	// guardo el pedido
    	$idPedido = $em->getRepository('PedidoBundle:Pedido')->insertPedido($pedido);
    	if ($idPedido) {
    		// elimino el pedido de la sesion
    		$sesion->set('pedido', null);    		
    		//$this->get('session')->setFlash('notice', 'Pedido realizado');
    		return $this->redirect($this->generateUrl('menu_pedido'));    		
    	} else {
    		// elimino el cliente
    		$em->remove($entity);
    		$em->flush();
    	}    	
    }
    
    /**
     * Método para actualizar un pedido
     * @param integer $id
     */
    public function updateAction ($id) {
    	 
    	$em = $this->getDoctrine()->getEntityManager();
    	
    	//recupero la informacion del pedido a editar
    	$entity = $em->getRepository('PedidoBundle:Pedido')->findPedidoById($id);
    	if (!$entity) {
    		throw $this->createNotFoundException('No se ha podido encontrar el pedido.');
    	}
    	// recojo los valores para el importe pagado y el estado
    	$request = $this->getRequest();
		$estado = intval($request->request->get('pedido_estado', $entity->getEstado()));
		$pagado = floatval($request->request->get('pedido_pagado', $entity->getPagado()));
    	// actualizo el pedido
    	$entity->setEstado($estado);
    	$entity->setPagado($pagado);
    	$em->persist($entity);
    	$em->flush();
    	
    	return $this->redirect($this->generateUrl('menu_pedido'));    	    
    }
}
