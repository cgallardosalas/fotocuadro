<?php

namespace FotoCuadro\PedidoBundle\Form;

use FotoCuadro\PedidoBundle\Entity\Pedido;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

/**
 * Clase que utilizamos para crear el formulario de creación de un pedido
 */
class PedidoType extends AbstractType
{
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Form.AbstractType::buildForm()
	 */
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
        	->add('id', 'hidden')
        	->add('usuario', 'entity', array('class' => 'FotoCuadro\UsuarioBundle\Entity\Usuario', 'property'=>'id', 'multiple'  => false, 'attr'=> array('style'=>'display:none') ))
        	->add('cliente', 'entity', array('class' => 'FotoCuadro\UsuarioBundle\Entity\Cliente', 'property'=>'id', 'multiple'  => false, 'attr'=> array('style'=>'display:none') ))
        	->add('fecha', 'text', array('required' => false))
        	->add('importe', 'text', array('required' => false))        	
        	->add('estado', 'choice', array('required' => true,
				        					'choices' => Pedido::getEstados(),
				        					'expanded' => false,
				        					'multiple' => false))
        	->add('incidencias', 'textarea', array('required' => false))        	        	
        ;
    }
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Form.FormTypeInterface::getName()
	 */
    public function getName()
    {
        return 'pedido';
    }
}
