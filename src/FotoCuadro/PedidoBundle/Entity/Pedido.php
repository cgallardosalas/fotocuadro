<?php

namespace FotoCuadro\PedidoBundle\Entity;
use FotoCuadro\UsuarioBundle\Entity\Usuario;
use FotoCuadro\UsuarioBundle\Entity\Cliente;
use Symfony\Component\Validator\Constraints\DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use FotoCuadro\UsuarioBundle\Lib\Util\Util;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\Mapping\Index;

/**
 * Entidad Pedido de FotoCuadro
 * @ORM\Table(name="pedido")
 * @ORM\Entity(repositoryClass="FotoCuadro\PedidoBundle\Entity\PedidoRepository") 
 */
class Pedido {
	
	const PEDIDO_CANCELADO = 0;
	const PEDIDO_PENDIENTE = 1;
	const PEDIDO_PAGADO = 2;
	const PEDIDO_PROVEEDOR = 3;
	const PEDIDO_ENTREGADO = 4;
	
	/**
	 * Identificador del pedido
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	/**
	 * Fecha del pedido
	 * @ORM\Column(type="datetime", nullable=false)
	 */
	protected $fecha;
	/**
	 * Usuario que crea el pedido
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="FotoCuadro\UsuarioBundle\Entity\Usuario", inversedBy="pedidos")
	 * @ORM\JoinColumn(name="usuario", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
	 */
	protected $usuario;
	/**
	 * Cliente que hace el pedido
	 * @ORM\ManyToOne(targetEntity="FotoCuadro\UsuarioBundle\Entity\Cliente", inversedBy="pedidos")
	 * @ORM\JoinColumn(name="cliente", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
	 */
	protected $cliente;
	/**
	 * Importe total del pedido 
	 * @ORM\Column(type="decimal", scale=2, nullable=true)
	 */
	protected $importe;
	/**
	 * Importe pagado del total del pedido
	 * @ORM\Column(type="decimal", scale=2, nullable=true)
	 */
	protected $pagado;
	/**
	 * Estado en que se encuentra el pedido
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $estado;
	/**
	 * Incidencias del pedido
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $incidencias;
	/**
	 * Lista con los productos del pedido
	 * @ORM\OneToMany(targetEntity="FotoCuadro\PedidoBundle\Entity\Producto", mappedBy="pedido", cascade={"persist", "remove"})
	 */
	protected $productos;
	

	/**
	 * Constructor de la clase. Inicia los valores por defecto
	 */
	public function __construct() {		
		$this->productos = new \Doctrine\Common\Collections\ArrayCollection();
		$this->estado = self::PEDIDO_PENDIENTE;
	}
	
	/**
	 * Método mágico para mostrar el ID del pedido
	 */
	public function __toString() {
		return $this->id;
	}

	/**
	 * Getter del usuario que crea el pedido
	 * @return Usuario
	 */
	public function getUsuario() {
		return $this->usuario;
	}
	/**
	 * Setter del usuario que crea el pedido
	 * @param Usuario $usuario
	 */
	public function setUsuario(Usuario $usuario) {
		$this->usuario = $usuario;
	}
	/**
	 * Getter para el ID del pedido
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}
	/**
	 * Getter para la fecha del pedido
	 * @return \DateTime
	 */
	public function getFecha() {
		return $this->fecha;
	}
	/**
	 * Setter para la fecha del pedido
	 * @param \DateTime $fecha
	 */
	public function setFecha(\DateTime $fecha) {
		$this->fecha = $fecha;
	}
	/**
	 * Getter para el cliente que hace el pedido
	 * @return Cliente
	 */
	public function getCliente() {
		return $this->cliente;
	}
	/**
	 * Setter para el cliente que hace el pedido
	 * @param Cliente $cliente
	 */
	public function setCliente($cliente) {
		$this->cliente = $cliente;
	}
	/**
	 * Getter para el importe total del pedido
	 * @return double
	 */
	public function getImporte() {
		return $this->importe;
	}
	/**
	 * Setter para el importe total del pedido
	 * @param double $importe
	 */
	public function setImporte($importe) {
		$this->importe = $importe;
	}
	/**
	 * Getter para el importe pagado del total del pedido
	 * @return double
	 */
	public function getPagado() {
		return $this->pagado;
	}
	/**
	 * Setter para el importe total pagado del pedido
	 * @param double $pagado
	 */
	public function setPagado($pagado) {
		$this->pagado = $pagado;
	}
	/**
	 * Getter para el estado del pedido
	 * @return int
	 */
	public function getEstado() {
		return $this->estado;
	}
	/**
	 * Setter para el estado del pedido
	 * @param int $estado
	 */
	public function setEstado($estado) {
		$this->estado = $estado;
	}
	/**
	 * Getter para las incidencias
	 * @return string
	 */
	public function getIncidencias() {
		return $this->incidencias;
	}
	/**
	 * Setter para las incidencias
	 * @param string $incidencias
	 */
	public function setIncidencias($incidencias) {
		$this->incidencias = $incidencias;
	}
	/**
	 * Getter para los productos del pedido
	 * @return array
	 */
	public function getProductos() {
		return $this->productos;
	}
	/**
	 * Método que añade un producto al pedido
	 * @param Producto $producto
	 */
	public function addProducto(Producto $producto) {
		$this->productos->add($producto);
	}
	
	/**
	 * Método que devuelve los estados de los pedidos
	 */
	public static function getEstados() {
		return array(
				self::PEDIDO_CANCELADO => 'Cancelado',
				self::PEDIDO_PENDIENTE => 'Pendiente',
				self::PEDIDO_PAGADO => 'Pagado',
				self::PEDIDO_PROVEEDOR => 'Proveedor',
				self::PEDIDO_ENTREGADO => 'Entregado');
	}
}
