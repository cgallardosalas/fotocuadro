<?php

namespace FotoCuadro\PedidoBundle\Entity;
use FotoCuadro\GaleriaBundle\Entity\Foto;
use Symfony\Component\Validator\Constraints\DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use FotoCuadro\UsuarioBundle\Lib\Util\Util;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\Mapping\Index;

/**
 * Entidad RolClub de FotoCuadro
 * @ORM\Table(name="producto") 
 * @ORM\Entity
 */
class Producto {

	/**
	 * Pedido al que pertence el producto
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="FotoCuadro\PedidoBundle\Entity\Pedido")
	 * @ORM\JoinColumn(name="pedido", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
	 */ 
	protected $pedido;
	/**
	 * Foto que se ha pedido
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="FotoCuadro\GaleriaBundle\Entity\Foto")
	 * @ORM\JoinColumn(name="foto", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
	 */
	protected $foto;
	/**
	 * Tipo de producto seleccionado para la foto
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="FotoCuadro\PedidoBundle\Entity\TipoProducto")
	 * @ORM\JoinColumn(name="tipo_producto", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
	 */
	protected $tipo_producto;
	/**
	 * Cantidad pedida 
	 * @ORM\Column(type="integer", nullable=false)
	 */
	protected $cantidad;

	/**
	 * Constructor de la clase. Inicia los valores por defecto
	 */
	public function __construct() {
		$this->cantidad = 0;
	}
	/**
	 * Getter para el pedido
	 * @return Pedido
	 */
	public function getPedido() {
		return $this->pedido;
	}
	/**
	 * Setter para el pedido
	 * @param Pedido $pedido
	 */
	public function setPedido(Pedido $pedido) {
		$this->pedido = $pedido;
	}
	/**
	 * Getter para la foto del pedido
	 * @return Foto
	 */
	public function getFoto() {
		return $this->foto;
	}
	/**
	 * Setter para la foto del pedido
	 * @param Foto $foto
	 */
	public function setFoto(Foto $foto) {
		$this->foto = $foto;
	}
	/**
	 * Getter para el tipo de producto
	 * @return TipoProducto
	 */
	public function getTipoProducto() {
		return $this->tipo_producto;
	}
	/**
	 * Setter para el tipo de producto
	 * @param TipoProducto $tipo_producto
	 */
	public function setTipoProducto($tipo_producto) {
		$this->tipo_producto = $tipo_producto;
	}
	/**
	 * Getter para la cantidad pedida para el producto
	 * @return int
	 */
	public function getCantidad() {
		return $this->cantidad;
	}
	/**
	 * Setter para la cantidad pedida para el producto
	 * @param int $cantidad
	 */
	public function setCantidad($cantidad) {
		$this->cantidad = $cantidad;
	}

}
