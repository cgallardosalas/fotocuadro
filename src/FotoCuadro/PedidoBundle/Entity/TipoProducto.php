<?php

namespace FotoCuadro\PedidoBundle\Entity;
use FotoCuadro\UsuarioBundle\Lib\Model\Desactivable;

use Symfony\Component\Validator\Constraints\DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use FotoCuadro\UsuarioBundle\Lib\Util\Util;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\Mapping\Index;

/**
 * Entidad TipoProducto de FotoCuadro
 * @ORM\Table(name="tipo_producto")
 * @ORM\Entity
 */
class TipoProducto implements Desactivable {

	/**
	 * Identificador de tipo de producto
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	protected $id;
	/**
	 * Nombre del tipo de producto
	 * @ORM\Column(type="string", length=60, nullable=false)
	 */
	protected $nombre;
	/**
	 * Precio del tipo de producto
	 * @ORM\Column(type="decimal", scale=2, nullable=true)
	 */
	protected $precio;
	/**
	 * Indicador de si el tipo de producto está activo o no
	 * @ORM\Column(type="boolean", nullable=false)
	 */
	protected $activo;

	/**
	 * Contructor de la clase.
	 * Crea los valores por defecto para el tipo de producto
	 */
	public function __construct() {
		$this->activo = true;
	}

	/**
	 * Método mágico toString para la clase, imprime el nombre del tipo de producto
	 */
	public function __toString() {
		return $this->nombre;

	}
	/**
	 * Getter para el ID del tipo de producto
	 * @return int 
	 */
	public function getId() {
		return $this->id;
	}
	/**
	 * Getter para el nombre del tipo de producto
	 * @return string
	 */
	public function getNombre() {
		return $this->nombre;
	}
	/**
	 * Setter para el nombre del tipo de producto
	 * @param string $nombre
	 */
	public function setNombre($nombre) {
		$this->nombre = $nombre;
	}
	/**
	 * Getter para el indicador de si el tipo de producto está activo o no
	 * @return boolean
	 */
	public function isActivo() {
		return $this->activo;
	}
	/**
	 * Método para desactivar el tipo de producto
	 */
	public function desactivar() {
		$this->activo = false;
	}
	/**
	 * Método para activar el tipo de producto
	 */
	public function activar() {
		$this->activo = true;
	}
	/**
	 * Getter para el precio del tipo de producto
	 * @return double
	 */
	public function getPrecio() {
		return $this->precio;
	}
	/**
	 * Setter para el precio del tipo de producto
	 * @param double $precio
	 */
	public function setPrecio($precio) {
		$this->precio = $precio;
	}

}
