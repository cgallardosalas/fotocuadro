<?php

namespace FotoCuadro\PedidoBundle\Entity;

use FotoCuadro\UsuarioBundle\Lib\Util\Util;

use FotoCuadro\UsuarioBundle\Entity\Usuario;
use FotoCuadro\UsuarioBundle\Lib\Util\DBConnection;
use Doctrine\ORM\EntityRepository;

/**
 * EntityRepository para el Pedido 
 * @author Conqueingredientes
 */
class PedidoRepository extends EntityRepository {
	
	/**
	 * Busca los pedidos de un usuario en funcion de su rol
	 * @param integer $idUsuario
	 */
	public function findPedidosByUser($usuario) {
		
		if ($usuario->getRol() == Usuario::ROL_ADMIN)
			return $this->findAllPedidos();
		else 
			return $this->findPedidosById($usuario->getId());		
	}
	
	/**
	 * Busca todos los pedidos de la base de datos
	 */
	public function findAllPedidos () {
		$em = $this->getEntityManager();
		
		$dql = "SELECT p, u, c
		FROM PedidoBundle:Pedido p
		JOIN p.usuario u JOIN p.cliente c		
		ORDER BY p.fecha DESC";
		
		$consulta = $em->createQuery($dql);
		$result = $consulta->getResult();
		$pedidos = array();
		
		foreach ($result as $pedido) {
			$pedidos[$pedido->getId()] = $pedido;
		}
		
		return $pedidos;
	}
	
	/**
	 * Busca todos los tipos de producto de la base de datos
	 */
	public function findAllTiposProducto () {
		$em = $this->getEntityManager();
	
		$dql = "SELECT tp
		FROM PedidoBundle:TipoProducto tp	
		WHERE tp.activo = 1	
		ORDER BY tp.id ASC";
	
		$consulta = $em->createQuery($dql);
	
		return $consulta->getResult();
	}
	
	/**
	 * Busca todos los tipos de producto de la base de datos
	 */
	public function findAllTiposProductoWithKeyId () {
		$em = $this->getEntityManager();
	
		$dql = "SELECT tp
		FROM PedidoBundle:TipoProducto tp
		WHERE tp.activo = 1
		ORDER BY tp.id ASC";
	
		$consulta = $em->createQuery($dql);
		$result = $consulta->getResult();

		$tipos = array();
		foreach ($result as $tipo) {
			$tipos[$tipo->getId()] = $tipo;
		}
	
		return $tipos;
	}	
	
	/**
	 * Busca todos los pedidos de un agente o de sus fotografos 
	 * @param integer $id
	 */
	public function findPedidosById ($id) {
		
		$ids[] = $id;
		
		$em = $this->getEntityManager();
					
		$dql = "SELECT u.id
		FROM UsuarioBundle:Usuario u
		WHERE u.creado_por = :idUsuario ";
		
		$consulta = $em->createQuery($dql);
		$consulta->setParameter('idUsuario', $id);
		$result = $consulta->getResult();						
		
		foreach ($result as $id)
			$ids[] = intval($id['id']);
						
		// cojo los pedidos realizado por el usuario o uno de sus fotografos
		$dql = "SELECT p, u, c
		FROM PedidoBundle:Pedido p
		JOIN p.usuario u JOIN p.cliente c
		WHERE p.usuario IN (".implode(',',$ids).")
		ORDER BY p.fecha DESC";			
		
		$consulta = $em->createQuery($dql);
		$result = $consulta->getResult();
		$pedidos = array();
		
		foreach ($result as $pedido) {
			$pedidos[$pedido->getId()] = $pedido;
		}
		
		return $pedidos;						
	}	

	/**
	 * Busca un pedido en funcion de su id
	 */
	public function findPedidoById ($id) {
		$em = $this->getEntityManager();
	
		$dql = "SELECT p, u, c
		FROM PedidoBundle:Pedido p
		JOIN p.usuario u JOIN p.cliente c		
		WHERE p.id = $id";
	
		$consulta = $em->createQuery($dql);
		return $consulta->getOneOrNullResult();		
	}
	
	/**
	 * Método que
	 * @return array
	 */
	public function infoPedidos () {
		
		$result = array ('pedidos_finalizados' => 0,
						'pedidos_pendientes' => 0,
						'pagos_realizados' => 0,
						'pagos_pendientes' => 0,
						'incidencias' => 0);
		
		$db_conn = DBConnection::connect();
		
		if ($db_conn) {
			// pedidos finalizados
			$sql = "SELECT count(*)
			FROM pedido
			WHERE estado = " . Pedido::PEDIDO_ENTREGADO;			
			$stmt = $db_conn->prepare($sql);
			$stmt->execute();
			$result['pedidos_finalizados'] = intval($stmt->fetchColumn());
			// pedidos pendiente
			$sql = "SELECT count(*)
			FROM pedido
			WHERE estado = " . Pedido::PEDIDO_PENDIENTE;				
			$stmt = $db_conn->prepare($sql);
			$stmt->execute();
			$result['pedidos_pendientes'] = intval($stmt->fetchColumn());
			// pagos realizados
			$sql = "SELECT count(*)
			FROM pedido
			WHERE importe = pagado";
			$stmt = $db_conn->prepare($sql);
			$stmt->execute();
			$result['pagos_realizados'] = intval($stmt->fetchColumn());
			// pagos pendientes
			$sql = "SELECT count(*)
			FROM pedido
			WHERE importe > pagado";
			$stmt = $db_conn->prepare($sql);
			$stmt->execute();
			$result['pagos_pendientes'] = intval($stmt->fetchColumn());
			// incidencias
			$sql = "SELECT count(*)
			FROM pedido
			WHERE incidencias != '' OR incidencias IS NULL";
			$stmt = $db_conn->prepare($sql);
			$stmt->execute();
			$result['incidencias'] = intval($stmt->fetchColumn());
			
			$db_conn = null;								
		}		
				
		return $result;
	}
	
	/**
	 * Inserta el pedido en la base de datos
	 * @param array $pedido
	 */
	public function insertPedido ($pedido) {
	
		ini_set('max_execution_time', 360);
	
		$db_conn = DBConnection::connect();
	
		if ($db_conn) {
			$db_conn->beginTransaction();				
	
			$sql = "INSERT INTO pedido (usuario, cliente, fecha, importe, estado, incidencias, pagado) " .
					"VALUES " .
					" (:usuario, :cliente, :fecha, :importe, :estado, :incidencias, :pagado) ";
	
			$stmt = $db_conn->prepare($sql);
			
			$stmt->bindValue('usuario', $pedido['usuario']);
			$stmt->bindValue('cliente', $pedido['cliente']);
			$stmt->bindValue('fecha', $pedido['fecha']->format('Y-m-d H:i:s'));
			$stmt->bindValue('importe', $pedido['importe']);
			$stmt->bindValue('estado', $pedido['estado']);
			$stmt->bindValue('incidencias', $pedido['incidencias']);
			$stmt->bindValue('pagado', $pedido['pagado']);			
			
			if (!$stmt->execute()) {									
				return false;
			}					
			$id = $db_conn->lastInsertId();
			
			foreach ($pedido['productos'] as $producto) {
					
				$sql2 = "INSERT INTO producto (pedido, foto, tipo_producto, cantidad) " .
					"VALUES " .
					" (:pedido, :foto, :tipo_producto, :cantidad) ";
	
				$stmt2 = $db_conn->prepare($sql2);
				
				$stmt2->bindValue('pedido', $id);
				$stmt2->bindValue('foto', $producto['foto']);
				$stmt2->bindValue('tipo_producto', $producto['tipo_producto']);
				$stmt2->bindValue('cantidad', $producto['cantidad']);				
				
				if (!$stmt2->execute()) {					
					return false;
				}				
			}
				
			$db_conn->commit();
	
			$db_conn = null;
			
			return $id;
		}
		return false;
	}
	
}
