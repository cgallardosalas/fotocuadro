<?php

namespace FotoCuadro\GaleriaBundle\Entity;
use FotoCuadro\UsuarioBundle\Lib\Model\Imprimible;

use Symfony\Component\Validator\Constraints\DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use FotoCuadro\UsuarioBundle\Lib\Util\Util;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\Mapping\Index;

/**
 * Entidad Foto de FotoCuadro
 * @ORM\Table(name="foto")
 * @ORM\Entity
 */
class Foto implements Imprimible {

	/**
	 * Identificador de la foto
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	private $id;
	/**
	 * Nombre de la foto
	 * @ORM\Column(type="string", length=100, nullable=false)
	 */
	private $nombre;
	/**
	 * Fecha de la foto
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $fecha;
	/**
	 * Archivo de la foto en disco
	 * @ORM\Column(type="string", length=250, nullable=false)
	 */
	private $archivo;
	/**
	 * Galería a la que pertenece la foto
	 * @ORM\ManyToOne(targetEntity="FotoCuadro\GaleriaBundle\Entity\Galeria", inversedBy="fotos")
	 * @ORM\JoinColumn(name="galeria", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
	 */
	private $galeria;
	/**
	 * Usuario que crea/sube la foto
	 * @ORM\ManyToOne(targetEntity="FotoCuadro\UsuarioBundle\Entity\Usuario", inversedBy="fotos")
	 * @ORM\JoinColumn(name="usuario", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
	 */
	private $usuario;

	/**
	 * Método mágico toString para la clase, imprime el nombre de la foto
	 */
	public function __toString() {
		return $this->nombre;

	}
	/**
	 * Getter para el ID de la galeria
	 * @return int 
	 */
	public function getId() {
		return $this->id;
	}
	/**
	 * Getter para el nombre de la galería
	 * @return string
	 */
	public function getNombre() {
		return $this->nombre;
	}
	/**
	 * Setter para el nombre  de la galería
	 * @param string $nombre
	 */
	public function setNombre($nombre) {
		$this->nombre = $nombre;
	}
	/**
	 * Getter para la fecha de la foto
	 * @return \DateTime
	 */
	public function getFecha() {
		return $this->fecha;
	}
	/**
	 * Setter para la fecha de la foto
	 * @param \DateTime $fecha
	 */
	public function setFecha(\DateTime $fecha) {
		$this->fecha = $fecha;
	}
	/**
	 * Getter para la ruta al archivo de la foto
	 * @return string
	 */
	public function getArchivo() {
		return $this->archivo;
	}
	/**
	 * Setter para la ruta al archivo de la foto
	 * @param string $archivo
	 */
	public function setArchivo($archivo) {
		$this->archivo = $archivo;
	}
	/**
	 * Getter para la galería a la que pertenece la foto
	 * @return Galeria
	 */
	public function getGaleria() {
		return $this->galeria;
	}
	/**
	 * Setter para la galería a la que pertenece la foto
	 * @param Galeria $galeria
	 */
	public function setGaleria(Galeria $galeria) {
		$this->galeria = $galeria;
	}
	/**
	 * Getter para el usuario propietario de la foto
	 * @return Usuario
	 */
	public function getUsuario() {
		return $this->usuario;
	}
	/**
	 * Setter para el usuario propietario de la foto
	 * @param Usuario $usuario
	 */
	public function setUsuario($usuario) {
		$this->usuario = $usuario;
	}
	/**
	 * Imprimir en PDF la galería
	 */
	public function imprimir() {
	
	}
}
