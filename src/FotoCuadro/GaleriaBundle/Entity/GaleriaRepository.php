<?php
namespace FotoCuadro\GaleriaBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * EntityRepository para Galeria 
 * @author Conqueingredientes
 */
class GaleriaRepository extends EntityRepository {
	/**
	 * Busca las galerías de una categoría
	 * @param integer $id ID de la categoría
	 */
	public function findGaleriasByIdCategoria($id)
	{
		$em = $this->getEntityManager();
	
		$dql = "SELECT g
		FROM GaleriaBundle:Galeria g
		WHERE g.categoria = :id";
	
		$consulta = $em->createQuery($dql);
		$consulta->setParameter('id', $id);
	
		return $consulta->getResult();
	}
	
	/**
	 * Busca una galería en función de su id
	 * @param integer $id ID de la galería
	 */
	public function findGaleriaById($id)
	{
		$em = $this->getEntityManager();
	
		$dql = "SELECT g
		FROM GaleriaBundle:Galeria g
		WHERE g.id = :id";
	
		$consulta = $em->createQuery($dql);
		$consulta->setParameter('id', $id);
	
		return $consulta->getOneOrNullResult();
	}
	
	/**
	 * Busca una foto en función de su id
	 * @param integer $id ID de la foto
	 */
	public function findFotoById($id)
	{
		$em = $this->getEntityManager();
	
		$dql = "SELECT f, g
		FROM GaleriaBundle:Foto f
		JOIN f.galeria g
		WHERE f.id = :id";
	
		$consulta = $em->createQuery($dql);
		$consulta->setParameter('id', $id);
	
		return $consulta->getOneOrNullResult();
	}
	
	/**
	 * Busca una lista de fotos en funcion de sus ids
	 * @param array $lista lista de los ids de las fotos
	 */
	public function findListaFotosById($lista)
	{
		$em = $this->getEntityManager();
	
		$dql = "SELECT f, g
		FROM GaleriaBundle:Foto f
		JOIN f.galeria g
		WHERE f.id IN (" . implode(",", $lista) . ")";
	
		$consulta = $em->createQuery($dql);		
	
		return $consulta->getResult();
	}
}
