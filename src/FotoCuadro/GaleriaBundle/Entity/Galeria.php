<?php

namespace FotoCuadro\GaleriaBundle\Entity;
use FotoCuadro\PedidoBundle\Entity\Pedido;

use FotoCuadro\ClubBundle\Entity\Categoria;

use FotoCuadro\UsuarioBundle\Lib\Model\Imprimible;

use Symfony\Component\Validator\Constraints\DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use FotoCuadro\UsuarioBundle\Lib\Util\Util;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\Mapping\Index;

/**
 * Entidad Galeria de FotoCuadro
 * @ORM\Table(name="galeria", uniqueConstraints={@UniqueConstraint(name="galeria_idx", columns={"categoria", "nombre"})})
 * @ORM\Entity(repositoryClass="FotoCuadro\GaleriaBundle\Entity\GaleriaRepository")
 */

class Galeria implements Imprimible {

	/**
	 * Identificador de la galeria
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	private $id;
	/**
	 * Nombre de la galeria
	 * @ORM\Column(type="string", length=50, nullable=false)
	 */
	private $nombre;
	/**
	 * Fecha de la galería
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $fecha;
	/**
	 * Categoría a la que pertenece la galería
	 * @ORM\ManyToOne(targetEntity="FotoCuadro\ClubBundle\Entity\Categoria", inversedBy="galerias")
	 * @ORM\JoinColumn(name="categoria", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
	 */
	private $categoria;
	/**
	 * Lista con las fotos de la galeria
	 * @ORM\OneToMany(targetEntity="FotoCuadro\GaleriaBundle\Entity\Foto", mappedBy="galeria", cascade={"persist", "remove"})
	 */
	private $fotos;

	/**
	 * Contructor de la clase.
	 * Crea los valores por defecto para la galeria
	 */
	public function __construct() {
		$this->fotos = new \Doctrine\Common\Collections\ArrayCollection();
		$this->activo = true;
	}

	/**
	 * Método mágico toString para la clase, imprime el nombre de la galería
	 */
	public function __toString() {
		return $this->nombre;		
	}
	/**
	 * Getter para el ID de la galeria
	 * @return int 
	 */
	public function getId() {
		return $this->id;
	}
	/**
	 * Getter para el nombre de la galería
	 * @return string
	 */
	public function getNombre() {
		return $this->nombre;
	}
	/**
	 * Setter para el nombre  de la galería
	 * @param string $nombre
	 */
	public function setNombre($nombre) {
		$this->nombre = $nombre;
	}

	/**
	 * Getter para las fotos de la galería
	 * @return array
	 */
	public function getFotos() {
		return $this->fotos;
	}
	/**
	 * Método que añade una foto
	 * @param Foto $foto
	 */
	public function addFoto(Foto $foto) {
		$this->fotos->add($foto);
	}
	/**
	 * Imprimir en PDF la galería
	 */
	public function imprimir() {

	}
	/**
	 * Getter para la fecha de la galería
	 * @return \DateTime
	 */
	public function getFecha() {
		return $this->fecha;
	}
	/**
	 * Setter de la fecha de la galería
	 * @param \DateTime $fecha
	 */
	public function setFecha(\DateTime $fecha) {
		$this->fecha = $fecha;
	}
	/**
	 * Getter para la categoría a la que pertenece la galería
	 * @return Categoria
	 */
	public function getCategoria() {
		return $this->categoria;
	}
	/**
	 * Setter para la categoría a la que pertenece la galería
	 * @param Categoria $categoria
	 */
	public function setCategoria(Categoria $categoria) {
		$this->categoria = $categoria;
	}
	
	/**
	 * Método que obtiene la imagen de la portada
	 */
	public function getPortada () {		
		
		if (!empty($this->fotos)) {
			$foto = $this->fotos->first();
			if ($foto)
				return strval($this->getId()) . "/" . $foto->getArchivo();
		}
		return "nodisponible.jpg";				
	}

}
