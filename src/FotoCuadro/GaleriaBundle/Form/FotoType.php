<?php

namespace FotoCuadro\GaleriaBundle\Form;

use FotoCuadro\GaleriaBundle\Entity\Foto;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

/**
 * Clase que utilizamos para crear el formulario de creación de una foto
 */
class FotoType extends AbstractType
{
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Form.AbstractType::buildForm()
	 */
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
        	->add('nombre', 'text', array('required' => false))        	                                                        
            ->add('archivo', 'file', array('required' => true, "attr" => array(
            		"accept" => "image/*",)))
            ->add('galeria', 'entity', array('class' => 'FotoCuadro\GaleriaBundle\Entity\Galeria', 'property'=>'id', 'multiple'  => false, 'attr'=> array('style'=>'display:none') ))
        ;
    }
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Form.FormTypeInterface::getName()
	 */
    public function getName()
    {
        return 'foto';
    }
}
