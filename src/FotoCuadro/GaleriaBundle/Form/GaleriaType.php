<?php

namespace FotoCuadro\GaleriaBundle\Form;

use FotoCuadro\GaleriaBundle\Entity\Galeria;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

/**
 * Clase que utilizamos para crear el formulario de creación de un club
 */
class GaleriaType extends AbstractType
{
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Form.AbstractType::buildForm()
	 */
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
        	->add('nombre', 'text', array('required' => true))        	
        	->add('categoria', 'entity', array('class' => 'FotoCuadro\ClubBundle\Entity\Categoria', 'property'=>'id', 'multiple'  => false, 'attr'=> array('style'=>'display:none') ))
        ;
    }
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Form.FormTypeInterface::getName()
	 */
    public function getName()
    {
        return 'galeria';
    }
}
