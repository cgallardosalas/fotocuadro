<?php

namespace FotoCuadro\GaleriaBundle\Controller;

use FotoCuadro\UsuarioBundle\Lib\Imagen;

use FotoCuadro\GaleriaBundle\Entity\Foto;

use FotoCuadro\GaleriaBundle\Form\FotoType;

use FotoCuadro\UsuarioBundle\Lib\Util\Util;

use FotoCuadro\GaleriaBundle\Form\GaleriaType;

use FotoCuadro\GaleriaBundle\Entity\Galeria;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{    
	/**
	 * Acción para crear una galeria
	 */
	public function createAction()
	{		
		// creo la galeria
		$entity = new Galeria();
		// obtengo la informacion de la request
		$request = $this->getRequest();	
		// creo el fomulario y uno los resultados
		$form = $this->createForm(new GaleriaType(), $entity);
		$form->bindRequest($request);
		// si es valido inserto en la base de datos
		if ($form->isValid()) {
			$em = $this->getDoctrine()->getEntityManager();
			// establezco la fecha de creación
			$entity->setFecha(new \DateTime());							
			// persisto la galeria
			try {
				$em->persist($entity);
				$em->flush();
			} catch (\Exception $e) {
				$this->get('session')->setFlash('notice', 'El nombre de la galería ya está siendo utilizado');				
			}
			try {
				// creo la carpeta en la que se guardaran las fotos
				$ruta_galeria = __DIR__ . "/../../../../web/fotos/galeria/";
				// creo la carpeta de la galeria
				mkdir($ruta_galeria . strval($entity->getId()), 0757);
			} catch (\Exception $e) {				
				$this->get('session')->setFlash('notice', 'No se ha podido crear la carpeta para la galería');
			}						
		}
		
		return $this->redirect($this->generateUrl('categoria_galerias', 
										array(
    										'club' => $entity->getCategoria()->getClub()->getId(),
    										'id' => $entity->getCategoria()->getId()))); 		
	}
	
	/**
	 * Acción para eliminar una galeria
	 * @param int $id
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function deleteAction($id) {
		$em = $this->getDoctrine()->getEntityManager();
		//recupero la informacion de la galeria a eliminar
		$entity = $em->getRepository('GaleriaBundle:Galeria')->findGaleriaById($id);
	
		if (!$entity) {
			throw $this->createNotFoundException('No se ha podido encontrar la galeria.');
		}
		$categoria = $entity->getCategoria()->getId();
		$club = $entity->getCategoria()->getClub()->getId();
	
		$em->remove($entity);
		$em->flush();
	
		return $this->redirect($this->generateUrl('categoria_galerias',
				array(
						'club' => $club,
						'id' => $categoria)));		
	}
	
	/**
	 * Acción para mostrar las fotos de una galeria
	 * @param integer $id
	 */
	public function showAction($id)
	{
		$em = $this->getDoctrine()->getEntityManager();
		//recupero la informacion de la galeria a mostrar
		$entity = $em->getRepository('GaleriaBundle:Galeria')->findGaleriaById($id);
		if (!$entity) {
			throw $this->createNotFoundException('No se ha podido encontrar la galería.');
		}
		// creo el formulario de creacion de fotos
		$form = $this->createForm(new FotoType(), new Foto());
		// muestro el formulario
		return $this->render('GaleriaBundle:Default:fotos.html.twig',
				array(
						'form' => $form->createView(),
						'galeria' => $entity,
				));
	
	}
	
	/**
	 * Acción para eliminar una foto
	 * @param int $id
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function deleteFotoAction($id) {
		$em = $this->getDoctrine()->getEntityManager();
		//recupero la informacion de la foto a eliminar
		$entity = $em->getRepository('GaleriaBundle:Galeria')->findFotoById($id);
	
		if (!$entity) {
			throw $this->createNotFoundException('No se ha podido encontrar la foto.');
		}		
		
		$galeria = $entity->getGaleria()->getId();
		$em->remove($entity);
		$em->flush();
		
		return $this->redirect($this->generateUrl('galeria_show',
												  array('id' => $galeria)));
	}
	
	/**
	 * Acción para crear fotos
	 */
	public function createFotoAction()
	{
		// creo la foto
		$entity = new Foto();
		// obtengo la informacion de la request
		$request = $this->getRequest();
		/*Util::preVardump($request);
		die();*/ 		
		// creo el fomulario y uno los resultados
		$form = $this->createForm(new FotoType(), $entity);
		$form->bindRequest($request);
		// si es valido inserto en la base de datos
		if ($form->isValid()) {
			$em = $this->getDoctrine()->getEntityManager();
			// establezco la fecha de creación
			$entity->setFecha(new \DateTime());
			// guardo la foto si la sube
			$files = $request->files->get('foto', null);
			if (!is_null($files['archivo'])) {
				$fileUpload = $files['archivo'];
				$entity->setArchivo(strtolower($fileUpload->getClientOriginalName()));
				// falta meterle la ruta de la galeria
				Imagen::guardarImagen($fileUpload, Imagen::IMAGEN_GALERIA, $entity->getGaleria()->getId());
				$nombre = explode(".", $entity->getArchivo());
				$entity->setNombre($nombre[0]);
			}
			// creo la relacion entre el usuario y el club
			$user = $this->get('security.context')->getToken()->getUser();
			$entity->setUsuario($user);
			// persisto la foto
			try {
				$em->persist($entity);
				$em->flush();
			} catch (\Exception $e) {
				$this->get('session')->setFlash('notice', 'No se ha podido guardar la foto');
			}					
		}
		
		return $this->redirect($this->generateUrl('galeria_show',
				array('id' => $entity->getGaleria()->getId())));
	}
}
