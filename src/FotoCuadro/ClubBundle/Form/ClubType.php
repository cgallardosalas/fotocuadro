<?php

namespace FotoCuadro\ClubBundle\Form;

use FotoCuadro\ClubBundle\Entity\Club;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

/**
 * Clase que utilizamos para crear el formulario de creación de un club
 */
class ClubType extends AbstractType
{
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Form.AbstractType::buildForm()
	 */
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
        	->add('nombre', 'text', array('required' => true))
        	->add('direccion', 'textarea', array('label' => 'Dirección', 'required' => false))        	
        	->add('email', 'email', array('required' => true))
        	->add('telefono', 'text', array('label' => 'Teléfono', 'required' => false))
        	->add('ciudad', 'text', array('required' => false))
            ->add('activo', 'checkbox', array('required' => false))                                                         
            ->add('imagen', 'file', array('required' => false, "attr" => array(
            		"accept" => "image/*")))
            ->add('categorias', 'collection', array(
            		'type' => new CategoriaType(),
            		'allow_add' => true,
            		'by_reference' => false,            		
            ))
        ;
    }
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Form.FormTypeInterface::getName()
	 */
    public function getName()
    {
        return 'club';
    }
}
