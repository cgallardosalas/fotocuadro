<?php

namespace FotoCuadro\ClubBundle\Form;

use FotoCuadro\ClubBundle\Entity\Categoria;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

/**
 * Clase que utilizamos para crear el formulario de creación de una categoria
 */
class CategoriaType extends AbstractType
{
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Form.AbstractType::buildForm()
	 */
    public function buildForm(FormBuilder $builder, array $options)
    {
    	// ver las opciones que tengo    	
        $builder
        	->add('id', 'hidden') 
        	->add('nombre', 'text', array('required' => true))        	
        	->add('categoria', 'choice', array(
        			'required' => true,
        			'choices' => Categoria::getCategorias(),
        			'expanded' => false,
        			'multiple' => false))  			        		        	
        	->add('club', 'entity', array('class' => 'FotoCuadro\ClubBundle\Entity\Club', 'property'=>'id', 'multiple'  => false, 'attr'=> array('style'=>'display:none') ))        	        	                                                                     
        ;
                
    }
	/**
	 * (non-PHPdoc)
	 * @see Symfony\Component\Form.FormTypeInterface::getName()
	 */
    public function getName()
    {
        return 'categoria';
    }
        

}
