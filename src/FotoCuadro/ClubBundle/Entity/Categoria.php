<?php

namespace FotoCuadro\ClubBundle\Entity;
use FotoCuadro\UsuarioBundle\Lib\Model\Desactivable;
use FotoCuadro\GaleriaBundle\Entity\Galeria;
use Symfony\Component\Validator\Constraints\DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use FotoCuadro\UsuarioBundle\Lib\Util\Util;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\Mapping\Index;

/**
 * Entidad Categoría de FotoCuadro
 * @ORM\Table(name="categoria")
 * @ORM\Entity 
 */
class Categoria implements Desactivable {

	const CATEGORIA_BENJAMIN = 1;
	const CATEGORIA_ALEVIN = 2;
	const CATEGORIA_INFANTIL = 3;
	const CATEGORIA_CADETE = 4;
	const CATEGORIA_JUVENIL = 5;
	const CATEGORIA_SENIOR = 6;
	
	/**
	 * Identificador de la categoria
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	private $id;
	/**
	 * Nombre de la categoria
	 * @ORM\Column(type="string", length=60, nullable=false)
	 */
	private $nombre;
	/**
	 * Indicador de si la categoria está activa o no
	 * @ORM\Column(type="boolean", nullable=false)
	 */
	private $activo;
	/**
	 * Categoría a la que pertenece 
	 * @ORM\Column(type="integer", nullable=false)
	 */
	private $categoria;
	/**
	 * Club al que pertence la categoría
	 * @ORM\ManyToOne(targetEntity="FotoCuadro\ClubBundle\Entity\Club", inversedBy="categorias")
	 * @ORM\JoinColumn(name="club", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
	 */
	private $club;
	/**
	 * Lista con las galerías de la categoría
	 * @ORM\OneToMany(targetEntity="FotoCuadro\GaleriaBundle\Entity\Galeria", mappedBy="categoria", cascade={"persist", "remove"})
	 */
	private $galerias;

	/**
	 * Contructor de la clase.
	 * Crea los valores por defecto para la categoría
	 */
	public function __construct() {
		$this->galerias = new \Doctrine\Common\Collections\ArrayCollection();
		$this->activo = true;
	}
	/**
	 * Método mágico toString para la clase, imprime el nombre de la categoría
	 */
	public function __toString() {
		return $this->nombre;

	}
	/**
	 * Getter para el ID de la categoría
	 * @return int 
	 */
	public function getId() {
		return $this->id;
	}
	/**
	 * Setter para el ID de la categoría
	 * @param int $id
	 */
	public function setId($id) {
		$this->id = $id;
	}
	/**
	 * Getter para el nombre
	 * @return string
	 */
	public function getNombre() {
		return $this->nombre;
	}
	/**
	 * Setter para el nombre
	 * @param string $nombre
	 */
	public function setNombre($nombre) {
		$this->nombre = $nombre;
	}
	/**
	 * Getter para el indicador de si la categoría está activa o no
	 * @return boolean
	 */
	public function isActivo() {
		return $this->activo;
	}
	/**
	 * Método para desactivar la categoría
	 */
	public function desactivar() {
		$this->activo = false;
	}
	/**
	 * Método para activar la categoría
	 */
	public function activar() {
		$this->activo = true;
	}
	/**
	 * Getter para la categoría
	 * @return int
	 */
	public function getCategoria() {
		return $this->categoria;
	}
	/**
	 * Setter para la categoría
	 * @param int $categoria
	 */
	public function setCategoria($categoria) {
		$this->categoria = $categoria;
	}
	/**
	 * Getter para el club
	 * @return Club
	 */
	public function getClub() {
		return $this->club;
	}
	/**
	 * Setter para el club
	 * @param Club $club
	 */
	public function setClub(Club $club) {
		$this->club = $club;		
	}
	/**
	 * Getter para las galerias de la categoria
	 * @return array
	 */
	public function getGalerias() {
		return $this->galerias;
	}
	/**
	 * Método que añade una galeria
	 * @param Galeria $categoria
	 */
	public function addGaleria(Galeria $galeria) {
		$this->galerias->add($galeria);
	}
	
	/**
	 * Método que devuelve las categorías de un club
	 */
	public static function getCategorias() {
		return array(
				self::CATEGORIA_BENJAMIN => 'Benjamín',
				self::CATEGORIA_ALEVIN => 'Alevín',
				self::CATEGORIA_INFANTIL => 'Infantil',
				self::CATEGORIA_CADETE => 'Cadete',
				self::CATEGORIA_JUVENIL => 'Juvenil',
				self::CATEGORIA_SENIOR => 'Senior');
	}
}
