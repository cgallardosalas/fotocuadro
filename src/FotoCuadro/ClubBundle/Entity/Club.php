<?php

namespace FotoCuadro\ClubBundle\Entity;
use FotoCuadro\UsuarioBundle\Lib\Model\Desactivable;

use Symfony\Component\Validator\Constraints\DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use FotoCuadro\UsuarioBundle\Lib\Util\Util;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\Mapping\Index;

/**
 * Entidad Club de FotoCuadro
 * @ORM\Table(name="club")
 * @ORM\Entity(repositoryClass="FotoCuadro\ClubBundle\Entity\ClubRepository")
 */
class Club implements Desactivable {

	/**
	 * Identificador del club
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue
	 */
	private $id;
	/**
	 * Nombre del club
	 * @ORM\Column(type="string", length=100, nullable=false)
	 */
	private $nombre;
	/**
	 * Direccion del club
	 * @ORM\Column(type="string", length=250, nullable=true)
	 */
	private $direccion;
	/**
	 * Email del club
	 * @ORM\Column(type="string", length=60, nullable=true)
	 */
	private $email;
	/**
	 * Teléfono del club
	 * @ORM\Column(type="string", length=20, nullable=true)
	 */
	private $telefono;
	/**
	 * Ciudad donde se encuentra el club
	 * @ORM\Column(type="string", length=80, nullable=true)
	 */
	private $ciudad;
	/**
	 * Indicador de si el club está activo o no
	 * @ORM\Column(type="boolean", nullable=false)
	 */
	private $activo;
	/**
	 * Fecha de creación del club
	 * @ORM\Column(type="datetime", nullable=false)
	 */
	private $creado;
	/**
	 * Imagen del club: logo etc
	 * @ORM\Column(type="string", length=250, nullable=true)
	 */
	private $imagen;
	/**
	 * Lista con las categorías del club
	 * @ORM\OneToMany(targetEntity="FotoCuadro\ClubBundle\Entity\Categoria", mappedBy="club", cascade={"remove"})
	 */
	private $categorias;
	
	/**
	 * Contructor de la clase.
	 * Crea los valores por defecto para el club
	 */
	public function __construct() {
		$this->categorias = new \Doctrine\Common\Collections\ArrayCollection();		
		$this->activo = true;
	}

	/**
	 * Método mágico toString para la clase, imprime el nombre del club
	 */
	public function __toString() {
		return $this->nombre;

	}
	/**
	 * Getter para el ID del club
	 * @return int 
	 */
	public function getId() {
		return $this->id;
	}
	/**
	 * Getter para el nombre del club
	 * @return string
	 */
	public function getNombre() {
		return $this->nombre;
	}
	/**
	 * Setter para el nombre del club
	 * @param string $nombre
	 */
	public function setNombre($nombre) {
		$this->nombre = $nombre;
	}
	/**
	 * Getter para el email
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}
	/**
	 * Setter para el email
	 * @param string $email
	 */
	public function setEmail($email) {
		$this->email = $email;
	}
	/**
	 * Getter para el telefono
	 * @return string
	 */
	public function getTelefono() {
		return $this->telefono;
	}
	/**
	 * Setter para el telefono
	 * @param string $telefono
	 */
	public function setTelefono($telefono) {
		$this->telefono = $telefono;
	}
	/**
	 * Getter para el indicador de si el club está activo o no
	 * @return boolean
	 */
	public function isActivo() {
		return $this->activo;
	}
	/**
	 * Getter para la fecha de creación del club
	 * @return \DateTime
	 */
	public function getCreado() {
		return $this->creado;
	}
	/**
	 * Setter para la fecha de creación del club
	 * @param \DateTime $creado
	 */
	public function setCreado(\DateTime $creado) {
		$this->creado = $creado;
	}
	/**
	 * Método para desactivar el club
	 */
	public function desactivar() {
		$this->activo = false;
	}
	/**
	 * Método para activar el club
	 */
	public function activar() {
		$this->activo = true;
	}
	/**
	 * Getter para la dirección del club
	 * @return string
	 */
	public function getDireccion() {
		return $this->direccion;
	}
	/**
	 * Setter para la dirección del club
	 * @param string $direccion
	 */
	public function setDireccion($direccion) {
		$this->direccion = $direccion;
	}
	/**
	 * Getter para el nombre de la ciudad
	 * @return string
	 */
	public function getCiudad() {
		return $this->ciudad;
	}
	/**
	 * Setter para el nombre de la ciduad
	 * @param string $ciudad
	 */
	public function setCiudad($ciudad) {
		$this->ciudad = $ciudad;
	}
	/**
	 * Getter para las categorías del club
	 * @return array
	 */
	public function getCategorias() {
		return $this->categorias;
	}
	/**
	 * Setter para las categorías del club
	 * @param array
	 */
	public function setCategorias(\Doctrine\Common\Collections\ArrayCollection $categorias) {
		/*echo "<pre>";
		var_dump($categorias);
		echo "</pre>";
		die();
		foreach ($categorias as $categoria) {
			
			$categoria->setClub($this);
		}*/
		
		$this->categorias = $categorias;
	}		
	/**
	 * Método que añade una categoría
	 * @param Categoria $categoria
	 */
	public function addCategoria(Categoria $categoria) {
		$this->categorias->add($categoria);
	}
	/**
	 * Getter para la imagen
	 * @return string
	 */
	public function getImagen() {
		return $this->imagen;
	}
	/**
	 * Setter para la imagen
	 * @param string $imagen
	 */
	public function setImagen($imagen) {
		$this->imagen = $imagen;
	}
}
