<?php

namespace FotoCuadro\ClubBundle\Entity;
use FotoCuadro\UsuarioBundle\Entity\Usuario;
use Symfony\Component\Validator\Constraints\DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use FotoCuadro\UsuarioBundle\Lib\Util\Util;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Doctrine\ORM\Mapping\Index;

/**
 * Entidad UsuarioClub de FotoCuadro
 * @ORM\Table(name="usuario_club") 
 * @ORM\Entity
 */
class UsuarioClub {

	/**
	 * Club sobre el que se aplica el rol
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="FotoCuadro\ClubBundle\Entity\Club")
	 * @ORM\JoinColumn(name="club", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
	 */ 
	private $club;
	/**
	 * Usuario que tiene el rol en el club
	 * @ORM\Id
	 * @ORM\ManyToOne(targetEntity="FotoCuadro\UsuarioBundle\Entity\Usuario")
	 * @ORM\JoinColumn(name="usuario", referencedColumnName="id", onDelete="CASCADE", onUpdate="CASCADE")
	 */
	private $usuario;	
	/**
	 * Getter del club sobre el que se tiene un rol
	 * @retun Club
	 */	
	public function getClub() {
		return $this->club;
	}
	/**
	 * Setter del club sobre el que se tiene un rol
	 * @param Club $club
	 */
	public function setClub(Club $club) {
		$this->club = $club;
	}
	/**
	 * Getter del usuario que tiene el rol
	 * @return Usuario
	 */
	public function getUsuario() {
		return $this->usuario;
	}
	/**
	 * Setter del usuario que tiene el rol
	 * @param Usuario $usuario
	 */
	public function setUsuario(Usuario $usuario) {
		$this->usuario = $usuario;
	}
}
