<?php
namespace FotoCuadro\ClubBundle\Entity;

use FotoCuadro\UsuarioBundle\Lib\Util\Util;

use Doctrine\ORM\EntityRepository;

/**
 * EntityRepository para Club 
 * @author Conqueingredientes
 */
class ClubRepository extends EntityRepository {
	/**
	 * Metodo que busca todos los clubes	 
	 * @return array
	 */
	public function findAllClubes()
	{
		$em = $this->getEntityManager();
	
		$dql = "SELECT c
		FROM ClubBundle:Club c
		WHERE c.activo = 1
		ORDER BY c.nombre";
	
		$consulta = $em->createQuery($dql);				
		$result = $consulta->getResult();
		$clubes = array();
		
		foreach ($result as $club) {
			$clubes[$club->getId()] = $club;
		}
		
		return $clubes;
	}
	
	/**
	 * Busca un club en función de su id
	 * @param integer $id ID del club
	 */
	public function findClubById($id)
	{
		$em = $this->getEntityManager();
	
		$dql = "SELECT c
		FROM ClubBundle:Club c		
		WHERE c.id = :id";
	
		$consulta = $em->createQuery($dql);
		$consulta->setParameter('id', $id);
	
		return $consulta->getOneOrNullResult();
	}
	
	/**
	 * Busca una categoria
	 * @param integer $id ID de la categoria
	 */
	public function findCategoriaById($id)
	{
		$em = $this->getEntityManager();
	
		$dql = "SELECT cat, g, c
		FROM ClubBundle:Categoria cat
		JOIN cat.galerias g
		JOIN cat.club c			
		WHERE cat.id = :id";
		//JOIN g.fotos f	
		$consulta = $em->createQuery($dql);
		$consulta->setParameter('id', $id);
	
		return $consulta->getOneOrNullResult();
	}
	
	/**
	 * Busca las categorias de un club
	 * @param integer $id ID del club
	 */
	public function findCategoriasByIdClub($id)
	{
		$em = $this->getEntityManager();
	
		$dql = "SELECT cat
		FROM ClubBundle:Categoria cat
		WHERE cat.club = :id";
	
		$consulta = $em->createQuery($dql);
		$consulta->setParameter('id', $id);
	
		return $consulta->getResult();
	}
	
	/**
	 * Selecciona los clubes en lso que está relacionado el usuario
	 * @param integer $id ID del usuario
	 */
	public function findClubesByUser($id)
	{
		$em = $this->getEntityManager();
		
		/*$dql = "SELECT uc, c
		FROM ClubBundle:UsuarioClub uc
		LEFT JOIN uc.club c
		WHERE uc.usuario = :idUsuario ";*/
		// cojo los ids de los clubes del usuario
		/*
		 * $query = $em->createQuery('SELECT u.id FROM CmsUser u WHERE EXISTS (SELECT p.phonenumber FROM CmsPhonenumber p WHERE p.user = u.id)');
		$ids = $query->getResult();
		 */
		$dql = "SELECT uc
		FROM ClubBundle:UsuarioClub uc		
		WHERE uc.usuario = :idUsuario ";
		
		$consulta = $em->createQuery($dql);
		$consulta->setParameter('idUsuario', $id);
		$result = $consulta->getResult();		
		
		$ids = array();		
		foreach ($result as $id)
			$ids[] = $id->getClub()->getId();
		
		// cojo los clubes con los id obtenidos
		$dql = "SELECT c
		FROM ClubBundle:Club c
		WHERE c.activo = 1 AND c.id IN (".implode(',',$ids).")";
						
		$consulta = $em->createQuery($dql);
		$result = $consulta->getResult();
		$clubes = array();
		
		foreach ($result as $club) {
			$clubes[$club->getId()] = $club;
		}

		return $clubes;
	}
	
}
