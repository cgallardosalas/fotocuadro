<?php

namespace FotoCuadro\ClubBundle\Controller;

use FotoCuadro\GaleriaBundle\Entity\Galeria;

use FotoCuadro\GaleriaBundle\Form\GaleriaType;

use FotoCuadro\ClubBundle\Entity\UsuarioClub;

use FotoCuadro\ClubBundle\Entity\Categoria;

use FotoCuadro\UsuarioBundle\Lib\Imagen;

use FotoCuadro\ClubBundle\Entity\Club;

use FotoCuadro\ClubBundle\Form\ClubType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FotoCuadro\UsuarioBundle\Entity\Usuario;

use FotoCuadro\UsuarioBundle\Lib\Util\Util;



class DefaultController extends Controller
{		
	/**
	 * Acción para el index del club, con los formulario y listados
	 */
    public function indexAction()
    {    	
    	// cojo los datos del usuario
    	$user = $this->get('security.context')->getToken()->getUser();
    	// obtener listado de equipos
    	$em = $this->getDoctrine()->getEntityManager();
    	if ($user->getRol() == Usuario::ROL_ADMIN) 
    		$lista = $em->getRepository('ClubBundle:Club')->findAllClubes();
    	else 
    		$lista = $em->getRepository('ClubBundle:Club')->findClubesByUser($user->getId());
    	    	
    	// si el usuario no es un fotografo
    	if ($user->getRol() != Usuario::ROL_FOTOGRAFO) {    		
    		$form = $this->createForm(new ClubType(), new Club());
    		// muestro el formulario
    		return $this->render('ClubBundle:Default:index.html.twig',
    			array(    
    					'form' => $form->createView(),					
    					'lista' => $lista,
    					'rol' => $user->getRol(), 
    			));
    	} else {
    		return $this->render('ClubBundle:Default:index.html.twig',
    			array(    
    					'form' => null,					
    					'lista' => $lista,
    					'rol' => $user->getRol(), 
    			));
    	}
    }

    /**
     * Acción para crear un club
     */
    public function createAction()
    {    	
    	// creo el club
    	$entity = new Club();
    	// obtengo la informacion de la request
    	$request = $this->getRequest();  
    	
    	$categorias = $request->request->get("club");
    	$categorias = isset($categorias["categorias"]) ? $categorias["categorias"] : array();    	
    	// creo el fomulario y uno los resultados
    	$form = $this->createForm(new ClubType(), $entity);
    	$form->bindRequest($request);
    	// si es valido inserto en la base de datos
    	if ($form->isValid()) {   
    		$em = $this->getDoctrine()->getEntityManager();    		
    		// establezco la fecha de creación
    		$entity->setCreado(new \DateTime());    		    	
    		// vacio las categorias ya que no funciona auto
    		$entity->setCategorias(new \Doctrine\Common\Collections\ArrayCollection());    		    	
    		// guardo la foto si la sube
    		$files = $request->files->get('club', null);
    		if (!is_null($files['imagen'])) {    			
    			$fileUpload = $files['imagen'];
    			$entity->setImagen(strtolower($fileUpload->getClientOriginalName()));
    			Imagen::guardarImagen($fileUpload, Imagen::IMAGEN_CLUB);
    		}    		
    		// persisto el club
    		$em->persist($entity);
    		$em->flush();    		
    		// por cada rol que se ha metido por usuario
    		foreach ($categorias as $categoria) {   
    			$new_cat = new Categoria();
    			$new_cat->setCategoria(intval($categoria['categoria']));
    			$new_cat->setNombre($categoria['nombre']);
    			$new_cat->setClub($entity);    			
    			$em->persist($new_cat);    			    		
    		}
    		$em->flush();    		
    		// creo la relacion entre el usuario y el club
    		$user = $this->get('security.context')->getToken()->getUser();
    		$uc = new UsuarioClub();
    		$uc->setClub($entity);
    		$uc->setUsuario($user);    		
    		$em->persist($uc);
    		$em->flush();
    	}
    	    	
    	return $this->redirect($this->generateUrl('menu_club'));
    }
    
    /**
     * Tranforma un array de objetos o de arrays en un array con indice
     * @param array $lista
     */
    private function getArrayWithKeys($lista) {
    	$result = array();
    	$i = 1;
    	foreach ($lista as $item) {
    		// si es desde el request
    		if (is_array($item)) {
    			if ($item['id'] != '')
    				$result[$item['id']] = $item;
    			else { 
    				$result["N$i"] = $item;
    				$i++;
    			}
    		} else {
    			$result[$item->getId()] = $item;
    		}    		
    	}
    	return $result;
    }
    /**
     * Método que actualiza la información del club
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateAction($id) {
    	$em = $this->getDoctrine()->getEntityManager();
    	// recupero la información del club
    	$entity = $em->getRepository('ClubBundle:Club')->findClubById($id);
    	if (!$entity) {
    		throw $this->createNotFoundException('No se ha podido encontrar el club.');
    	}   
    	// cojo el listado de categorias con el indice en el array
    	$current_cats = $this->getArrayWithKeys($entity->getCategorias());
    	// vacio las categorias 
    	$entity->setCategorias(new \Doctrine\Common\Collections\ArrayCollection()); 
    		
    	$editForm = $this->createForm(new ClubType(), $entity);    	        	
    	$request = $this->getRequest(); 
    	// recojo las categotrias del form
    	$categorias = $request->request->get("club");
    	$categorias = isset($categorias["categorias"]) ? $this->getArrayWithKeys($categorias["categorias"]) : array();    	
    	// busco las operaciones a realizar por cada lista
    	$cats_crear = array_diff_key($categorias,  $current_cats);
    	$cats_actualizar = array_intersect_key($current_cats, $categorias);
    	$cats_borrar = array_diff_key($current_cats, $categorias);
    	
    	// creo las nuevas
    	foreach ($cats_crear as $categoria) {
    		$new_cat = new Categoria();
    		$new_cat->setId(intval($categoria['id']));
    		$new_cat->setCategoria(intval($categoria['categoria']));
    		$new_cat->setNombre($categoria['nombre']);
    		$new_cat->setClub($entity);
    		$em->persist($new_cat);
    	}    	
    	// actualizo las que se han modificado
    	foreach ($cats_actualizar as $key => $categoria) {
    		$categoria->setCategoria(intval($categorias[$key]['categoria']));
    		$categoria->setNombre($categorias[$key]['nombre']);   
    		$em->persist($categoria);
    	}    	    
    	//elimino los que no estan en el form
    	foreach ($cats_borrar as $categoria) {
    		$em->remove($categoria);    		
    		/*$gategoria->desactivar();
    		$em->persist($gategoria);*/    		
    	}
    		        	
    	$em->flush();
    	$image = $entity->getImagen();
    	// actualizo la informacion
    	$editForm->bindRequest($request);
    	
    	if ($editForm->isValid()) {     	
    		$files = $request->files->get('club', null);
    		if (!is_null($files['imagen'])) {
    			$fileUpload = $files['imagen'];
    			$entity->setImagen(strtolower($fileUpload->getClientOriginalName()));
    			Imagen::guardarImagen($fileUpload, Imagen::IMAGEN_CLUB);
    		}  else
    			$entity->setImagen($image);
    		// vacio las categorias para que no persista y me de error ya que ya lo he persistido antes    		        	
    		$entity->setCategorias(new \Doctrine\Common\Collections\ArrayCollection());    		
    			
    		$em->persist($entity);
    		$em->flush();    		   	
    	}
    	return $this->redirect($this->generateUrl('menu_club'));
    }
    
    /**
     * Acción para editar un club
     * @param integer $id
     */
    public function editAction($id)
    {    	
    	$em = $this->getDoctrine()->getEntityManager();
    	//recupero la informacion del club a editar
    	$entity = $em->getRepository('ClubBundle:Club')->findClubById($id);    	    	    	
    	if (!$entity) {
    		throw $this->createNotFoundException('No se ha podido encontrar el club.');
    	}
    	// creo el formulario de edición
    	$editForm = $this->createForm(new ClubType(), $entity);    	     
    	// muestro el formulario
    	return $this->render('ClubBundle:Default:edit.html.twig',
    			array(
    					'form' => $editForm->createView(),
    					'entity' => $entity,
    			));
    	 
    }
    
    /**
     * Acción para eliminar un club
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($id) {
    	$em = $this->getDoctrine()->getEntityManager();
    	//recupero la informacion del club a editar
    	$entity = $em->getRepository('ClubBundle:Club')->findClubById($id);
    
    	if (!$entity) {
    		throw $this->createNotFoundException('No se ha podido encontrar el club.');
    	}
    
    	//$em->remove($entity);
    	$entity->desactivar();    	
    	$em->persist($entity);
    	$em->flush();
    
    	return $this->redirect($this->generateUrl('menu_club'));
    }
    
    /**
     * Acción para mostrar las galerías de una categoría de un club
     * @param integer $club
     * @param integer $id
     */
    public function galeriasAction ($club, $id) {
    	$em = $this->getDoctrine()->getEntityManager();
    	$categoria = $em->getRepository('ClubBundle:Club')->findCategoriaById($id);

    	if (!$categoria) {
    		throw $this->createNotFoundException('No se ha podido encontrar la categoria.');
    	}
    			
  		$form = $this->createForm(new GaleriaType(), new Galeria());
    	return $this->render('GaleriaBundle:Default:index.html.twig',
    			array(
    					'form' => $form->createView(),
    					'categoria' => $categoria,    					 				    					
    			));    	    	    	    	       
    }
    
}
