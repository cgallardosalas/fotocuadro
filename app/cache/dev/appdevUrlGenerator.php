<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;


/**
 * appdevUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appdevUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    static private $declaredRouteNames = array(
       '_wdt' => true,
       '_profiler_search' => true,
       '_profiler_purge' => true,
       '_profiler_import' => true,
       '_profiler_export' => true,
       '_profiler_search_results' => true,
       '_profiler' => true,
       '_configurator_home' => true,
       '_configurator_step' => true,
       '_configurator_final' => true,
       'pedido_edit' => true,
       'pedido_new_clubes' => true,
       'pedido_new_categorias' => true,
       'pedido_new_galerias' => true,
       'pedido_new_fotos' => true,
       'pedido_new_seleccion' => true,
       'pedido_guardar_producto' => true,
       'pedido_confirmar' => true,
       'pedido_crear' => true,
       'galeria_create' => true,
       'galeria_show' => true,
       'galeria_delete' => true,
       'foto_delete' => true,
       'foto_create' => true,
       'club_create' => true,
       'club_edit' => true,
       'club_update' => true,
       'club_delete' => true,
       'categoria_galerias' => true,
       'homepage' => true,
       'login_usuario' => true,
       'login_check_usuario' => true,
       'logout_usuario' => true,
       'usuario_create' => true,
       'usuario_edit' => true,
       'usuario_update' => true,
       'menu_panel' => true,
       'menu_pedido' => true,
       'menu_club' => true,
       'menu_usuario' => true,
       'menu_configuracion' => true,
    );

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function generate($name, $parameters = array(), $absolute = false)
    {
        if (!isset(self::$declaredRouteNames[$name])) {
            throw new RouteNotFoundException(sprintf('Route "%s" does not exist.', $name));
        }

        $escapedName = str_replace('.', '__', $name);

        list($variables, $defaults, $requirements, $tokens) = $this->{'get'.$escapedName.'RouteInfo'}();

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $absolute);
    }

    private function get_wdtRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::toolbarAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/_wdt',  ),));
    }

    private function get_profiler_searchRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_profiler/search',  ),));
    }

    private function get_profiler_purgeRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::purgeAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_profiler/purge',  ),));
    }

    private function get_profiler_importRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::importAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_profiler/import',  ),));
    }

    private function get_profiler_exportRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::exportAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '.txt',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/\\.]+?',    3 => 'token',  ),  2 =>   array (    0 => 'text',    1 => '/_profiler/export',  ),));
    }

    private function get_profiler_search_resultsRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchResultsAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/search/results',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  2 =>   array (    0 => 'text',    1 => '/_profiler',  ),));
    }

    private function get_profilerRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::panelAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/_profiler',  ),));
    }

    private function get_configurator_homeRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_configurator/',  ),));
    }

    private function get_configurator_stepRouteInfo()
    {
        return array(array (  0 => 'index',), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'index',  ),  1 =>   array (    0 => 'text',    1 => '/_configurator/step',  ),));
    }

    private function get_configurator_finalRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/_configurator/final',  ),));
    }

    private function getpedido_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::editAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/pedido/edit',  ),));
    }

    private function getpedido_new_clubesRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::newClubesAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/pedido/new/clubes/',  ),));
    }

    private function getpedido_new_categoriasRouteInfo()
    {
        return array(array (  0 => 'idClub',), array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::newCategoriasAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/categorias/',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'idClub',  ),  2 =>   array (    0 => 'text',    1 => '/pedido/new/clubes',  ),));
    }

    private function getpedido_new_galeriasRouteInfo()
    {
        return array(array (  0 => 'categoria',), array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::newGaleriasAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/galerias/',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'categoria',  ),  2 =>   array (    0 => 'text',    1 => '/pedido/new/clubes/categorias',  ),));
    }

    private function getpedido_new_fotosRouteInfo()
    {
        return array(array (  0 => 'galeria',), array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::newFotosAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/fotos/',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'galeria',  ),  2 =>   array (    0 => 'text',    1 => '/pedido/new/clubes/categorias/galerias',  ),));
    }

    private function getpedido_new_seleccionRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::seleccionFotosAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/pedido/new/clubes/categorias/galerias/fotos/',  ),));
    }

    private function getpedido_guardar_productoRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::guardarProductoAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/pedido/guardar/producto/',  ),));
    }

    private function getpedido_confirmarRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::confirmarAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/pedido/confirmar/',  ),));
    }

    private function getpedido_crearRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::crearAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/pedido/crear/',  ),));
    }

    private function getgaleria_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FotoCuadro\\GaleriaBundle\\Controller\\DefaultController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/galeria/create/',  ),));
    }

    private function getgaleria_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'FotoCuadro\\GaleriaBundle\\Controller\\DefaultController::showAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/galeria/show',  ),));
    }

    private function getgaleria_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'FotoCuadro\\GaleriaBundle\\Controller\\DefaultController::deleteAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/galeria/delete',  ),));
    }

    private function getfoto_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'FotoCuadro\\GaleriaBundle\\Controller\\DefaultController::deleteFotoAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/galeria/foto/delete',  ),));
    }

    private function getfoto_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FotoCuadro\\GaleriaBundle\\Controller\\DefaultController::createFotoAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/galeria/foto/create/',  ),));
    }

    private function getclub_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FotoCuadro\\ClubBundle\\Controller\\DefaultController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/club/create/',  ),));
    }

    private function getclub_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'FotoCuadro\\ClubBundle\\Controller\\DefaultController::editAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/club/edit',  ),));
    }

    private function getclub_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'FotoCuadro\\ClubBundle\\Controller\\DefaultController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/club/update',  ),));
    }

    private function getclub_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'FotoCuadro\\ClubBundle\\Controller\\DefaultController::deleteAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/club/delete',  ),));
    }

    private function getcategoria_galeriasRouteInfo()
    {
        return array(array (  0 => 'club',  1 => 'id',), array (  '_controller' => 'FotoCuadro\\ClubBundle\\Controller\\DefaultController::galeriasAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/galerias',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/categoria',  ),  3 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'club',  ),  4 =>   array (    0 => 'text',    1 => '/club',  ),));
    }

    private function gethomepageRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FotoCuadro\\UsuarioBundle\\Controller\\DefaultController::panelAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/',  ),));
    }

    private function getlogin_usuarioRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FotoCuadro\\UsuarioBundle\\Controller\\DefaultController::loginAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/login',  ),));
    }

    private function getlogin_check_usuarioRouteInfo()
    {
        return array(array (), array (), array (), array (  0 =>   array (    0 => 'text',    1 => '/login_check',  ),));
    }

    private function getlogout_usuarioRouteInfo()
    {
        return array(array (), array (), array (), array (  0 =>   array (    0 => 'text',    1 => '/logout',  ),));
    }

    private function getusuario_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FotoCuadro\\UsuarioBundle\\Controller\\DefaultController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/usuarios/usuario/create/',  ),));
    }

    private function getusuario_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'FotoCuadro\\UsuarioBundle\\Controller\\DefaultController::editAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/usuarios/usuario/edit',  ),));
    }

    private function getusuario_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'FotoCuadro\\UsuarioBundle\\Controller\\DefaultController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/usuarios/usuario/update',  ),));
    }

    private function getmenu_panelRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FotoCuadro\\UsuarioBundle\\Controller\\DefaultController::panelAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/',  ),));
    }

    private function getmenu_pedidoRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/pedidos/',  ),));
    }

    private function getmenu_clubRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FotoCuadro\\ClubBundle\\Controller\\DefaultController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/clubes/',  ),));
    }

    private function getmenu_usuarioRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FotoCuadro\\UsuarioBundle\\Controller\\DefaultController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/usuarios/',  ),));
    }

    private function getmenu_configuracionRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FotoCuadro\\UsuarioBundle\\Controller\\DefaultController::configuracionAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/configuracion/',  ),));
    }
}
