<?php

/* ::opciones.html.twig */
class __TwigTemplate_f25198fa2faef5e7582fb482c45e3a65 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("logout_usuario"), "html", null, true);
        echo "\"><img src=\"#\" /> Salir</a>
";
    }

    public function getTemplateName()
    {
        return "::opciones.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  17 => 1,);
    }
}
