<?php

/* ClubBundle:Default:index.html.twig */
class __TwigTemplate_4dc9ac88a411fba01ca8924257f32e63 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'contenido' => array($this, 'block_contenido'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Clubes";
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "        \t        \t        
";
    }

    // line 7
    public function block_contenido($context, array $blocks = array())
    {
        // line 8
        echo "\t<div class=\"contenido\">
\t\t<div class=\"title\">Clubes</div>
\t\t<div class=\"izquierda\">\t
\t\t\t";
        // line 11
        if ($this->getContext($context, "lista")) {
            echo "\t\t\t    \t\t\t
    \t\t\t";
            // line 13
            echo "    \t\t\t";
            $this->env->loadTemplate("ClubBundle:Default:widget/listado.html.twig")->display($context);
            // line 14
            echo "    \t\t";
        } else {
            // line 15
            echo "    \t\t\t<p>No hay clubes para mostrar</p>\t    \t\t\t
    \t\t";
        }
        // line 16
        echo "    \t\t
\t\t</div>
\t\t<div class=\"derecha\">
\t\t\t";
        // line 19
        if ($this->getContext($context, "form")) {
            // line 20
            echo "\t\t\t\t";
            // line 21
            echo "\t    \t\t";
            $this->env->loadTemplate("ClubBundle:Default:new.html.twig")->display($context);
            // line 22
            echo "    \t\t";
        }
        echo "\t    \t\t
\t\t</div>
\t\t
\t</div>\t\t\t\t\t\t\t
";
    }

    // line 28
    public function block_javascripts($context, array $blocks = array())
    {
        // line 29
        echo "\t";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
\t<script type=\"text/javascript\">
jQuery(document).ready(function() {
\t // Get the div that holds the collection of tags
\tvar collectionHolder = \$('ul.categorias');
\t// setup an \"add a tag\" link
\tvar \$addCategoriaLink = \$('<a href=\"#\" class=\"add_categoria_link\">A&ntilde;adir categor&iacute;a</a>');
\tvar \$newLinkLi = \$('<li></li>').append(\$addCategoriaLink);
\t    // add the \"add a tag\" anchor and li to the tags ul
\t    collectionHolder.append(\$newLinkLi);

\t    \$addCategoriaLink.on('click', function(e) {\t    \t
\t        // prevent the link from creating a \"#\" on the URL
\t        e.preventDefault();
\t        // add a new tag form (see next code block)
\t        addCategoriaForm(collectionHolder, \$newLinkLi);
\t    });
\t});

function addCategoriaForm(collectionHolder, \$newLinkLi) {
    // Get the data-prototype we explained earlier
    var prototype = collectionHolder.attr('data-prototype');
    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on the current collection's length.
    var newForm = prototype.replace(/\\\$\\\$name\\\$\\\$/g, collectionHolder.children().length);

    // Display the form in the page in an li, before the \"Add a tag\" link li
    var \$newFormLi = \$('<li></li>').append(newForm);
    \$newLinkLi.before(\$newFormLi);

\t // add a delete link to the new form
    addTagFormDeleteLink(\$newFormLi);
}

function addTagFormDeleteLink(\$tagFormLi) {
    var \$removeFormA = \$('<a href=\"#\">Eliminar</a>');
    \$tagFormLi.append(\$removeFormA);

    \$removeFormA.on('click', function(e) {
        // prevent the link from creating a \"#\" on the URL
        e.preventDefault();

        // remove the li for the tag form
        \$tagFormLi.remove();
    });
}

</script>\t\t
";
    }

    public function getTemplateName()
    {
        return "ClubBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 29,  89 => 28,  79 => 22,  76 => 21,  74 => 20,  72 => 19,  67 => 16,  63 => 15,  60 => 14,  57 => 13,  53 => 11,  48 => 8,  45 => 7,  38 => 4,  35 => 3,  29 => 2,);
    }
}
