<?php

/* ClubBundle:Default:edit.html.twig */
class __TwigTemplate_a462a3d86e859e08668c53b75eb0db60 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'contenido' => array($this, 'block_contenido'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Clubes";
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "        \t        \t        
";
    }

    // line 7
    public function block_contenido($context, array $blocks = array())
    {
        // line 8
        echo "\t<div class=\"contenido\">\t\t
\t\t<div class=\"title\">Clubes</div>
\t\t<div class=\"centro\">\t
    \t\t";
        // line 12
        echo "    \t\t<div class=\"sub-title\">Modificar club</div>
\t\t\t<form action=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("club_update", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id"))), "html", null, true);
        echo "\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo ">  \t\t\t
\t\t\t\t";
        // line 14
        echo $this->env->getExtension('form')->renderRow($this->getAttribute($this->getContext($context, "form"), "_token"));
        echo "
\t\t\t\t<!-- Formulario general de un club  -->
\t\t\t\t<div class=\"box_ficha\">
\t\t\t\t";
        // line 17
        echo $this->env->getExtension('form')->renderErrors($this->getContext($context, "form"));
        echo "\t\t\t\t\t
\t\t\t\t";
        // line 18
        $context["readonly"] = ($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "user"), "rol") === constant("\\FotoCuadro\\UsuarioBundle\\Entity\\Usuario::ROL_FOTOGRAFO"));
        // line 19
        echo "\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td><strong>";
        // line 22
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "nombre"));
        echo "</strong></td>
\t\t\t\t\t\t<td>";
        // line 23
        echo (($this->getContext($context, "readonly")) ? ($this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "nombre"), array("attr" => array("size" => "40", "readonly" => "readonly")))) : ($this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "nombre"), array("attr" => array("size" => "40")))));
        echo "</td>\t\t\t\t\t\t 
\t\t\t\t\t</tr>
\t\t\t\t\t";
        // line 26
        echo "\t\t\t\t\t";
        if (($this->getAttribute($this->getContext($context, "entity"), "imagen") && ($this->getAttribute($this->getContext($context, "entity"), "imagen") != ""))) {
            echo "\t\t\t\t\t\t
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td>&nbsp;</td>
\t\t\t\t\t\t<td>
    \t\t\t\t\t\t<img class=\"imagen_club\" src=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(("fotos/club/" . $this->getAttribute($this->getContext($context, "entity"), "imagen"))), "html", null, true);
            echo "\" width=\"200\" />  \t\t\t
        \t\t\t\t\t<input name=\"ruta_imagen\" id=\"ruta_imagen\" type=\"hidden\" value=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "imagen"), "html", null, true);
            echo "\"/>
        \t\t\t\t</td>
        \t\t\t</tr>            \t                 
\t\t\t\t\t";
        }
        // line 34
        echo "\t
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td><strong>";
        // line 36
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "imagen"));
        echo "</strong></td>\t\t\t\t\t\t
\t\t\t\t\t\t<td>";
        // line 37
        echo (($this->getContext($context, "readonly")) ? ($this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "imagen"), array("attr" => array("size" => "40", "disabled" => "disabled")))) : ($this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "imagen"), array("attr" => array("size" => "40")))));
        echo "</td>\t\t
\t\t\t\t\t</tr>\t\t
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td><strong>";
        // line 40
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "direccion"));
        echo "</strong></td>
\t\t\t\t\t\t<td>";
        // line 41
        echo (($this->getContext($context, "readonly")) ? ($this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "direccion"), array("attr" => array("readonly" => "readonly")))) : ($this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "direccion"))));
        echo "</td>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td><strong>";
        // line 44
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "ciudad"));
        echo "</strong></td>
\t\t\t\t\t\t<td>";
        // line 45
        echo (($this->getContext($context, "readonly")) ? ($this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "ciudad"), array("attr" => array("readonly" => "readonly")))) : ($this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "ciudad"))));
        echo "</td>\t\t
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td><strong>";
        // line 48
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "telefono"));
        echo "</strong></td>
\t\t\t\t\t\t<td>";
        // line 49
        echo (($this->getContext($context, "readonly")) ? ($this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "telefono"), array("attr" => array("readonly" => "readonly")))) : ($this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "telefono"))));
        echo "</td>\t\t\t
\t\t\t\t\t</tr>\t\t
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td><strong>";
        // line 52
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "email"));
        echo "</strong></td>
\t\t\t\t\t\t<td>";
        // line 53
        echo (($this->getContext($context, "readonly")) ? ($this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "email"), array("attr" => array("readonly" => "readonly")))) : ($this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "email"))));
        echo "</td>\t
\t\t\t\t\t</tr>\t\t\t
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td><strong>";
        // line 56
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "activo"));
        echo "</strong></td>
\t\t\t\t\t\t<td>";
        // line 57
        echo (($this->getContext($context, "readonly")) ? ($this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "activo"), array("attr" => array("disabled" => "disabled")))) : ($this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "activo"))));
        echo "</td>\t
\t\t\t\t\t</tr>
\t\t\t\t</table>\t
\t\t\t\t<ul class=\"categorias\" data-prototype=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "categorias"), "vars"), "prototype")));
        echo "\">\t
\t\t\t        ";
        // line 62
        echo "\t\t\t        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "categorias"));
        foreach ($context['_seq'] as $context["_key"] => $context["categoria"]) {
            echo "        \t
\t\t\t            <li id=\"li";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "categoria"), "vars"), "value"), "id"), "html", null, true);
            echo "\">";
            echo $this->env->getExtension('form')->renderWidget($this->getContext($context, "categoria"));
            echo "
\t\t\t            ";
            // line 64
            if ((!$this->getContext($context, "readonly"))) {
                // line 65
                echo "\t\t\t            \t<a class=\"del_categoria\" id=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "categoria"), "vars"), "value"), "id"), "html", null, true);
                echo "\" href=\"#\">Eliminar</a>\t
\t\t\t            \t&nbsp;&nbsp;|&nbsp;&nbsp;<a href=\"";
                // line 66
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("categoria_galerias", array("club" => $this->getAttribute($this->getContext($context, "entity"), "id"), "id" => $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "categoria"), "vars"), "value"), "id"))), "html", null, true);
                echo "\">Galer&iacute;as</a>
\t\t\t            \t<br />\t\t            \t
\t\t\t            ";
            }
            // line 68
            echo " \t\t\t            
\t\t\t            </li>\t\t\t\t\t\t\t\t\t            
\t\t\t        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categoria'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 71
        echo "\t\t\t    </ul>    \t\t\t\t\t
\t\t\t\t";
        // line 72
        if ((!$this->getContext($context, "readonly"))) {
            // line 73
            echo "\t\t\t\t\t<button type=\"submit\">Aceptar</button>
\t\t\t\t\t<a title=\"Eliminar\" onclick=\"return confirm('¿Seguro que quieres eliminar el club?');\" href=\"";
            // line 74
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("club_delete", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id"))), "html", null, true);
            echo "\">
\t\t\t\t\t<button type=\"button\">Eliminar</button></a>
\t\t\t\t";
        }
        // line 76
        echo "\t\t\t\t\t \t  \t\t\t\t\t   \t\t\t\t
\t\t\t\t<button type=\"button\" onclick=\"location.href='";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("menu_club"), "html", null, true);
        echo "';\" value=\"volver\" name=\"volver\">Volver</button>
\t\t\t</form>
\t\t</div>\t\t
\t</div>\t\t\t\t\t\t\t
";
    }

    // line 82
    public function block_javascripts($context, array $blocks = array())
    {
        // line 83
        echo "\t";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "\t
\t<script type=\"text/javascript\">
jQuery(document).ready(function() {
\t // Get the div that holds the collection of tags
\tvar collectionHolder = \$('ul.categorias');
\t// setup an \"add a tag\" link
\tvar \$addCategoriaLink = \$('<br /><a href=\"#\" class=\"add_categoria_link\">A&ntilde;adir categoria</a>');
\tvar \$newLinkLi = \$('<li></li>').append(\$addCategoriaLink);
\t    // add the \"add a tag\" anchor and li to the tags ul
\t    collectionHolder.append(\$newLinkLi);

\t    \$addCategoriaLink.on('click', function(e) {\t    \t
\t        // prevent the link from creating a \"#\" on the URL
\t        e.preventDefault();
\t        // add a new tag form (see next code block)
\t        addCategoriaForm(collectionHolder, \$newLinkLi);
\t    });
\t});

function addCategoriaForm(collectionHolder, \$newLinkLi) {
    // Get the data-prototype we explained earlier
    var prototype = collectionHolder.attr('data-prototype');
    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on the current collection's length.
    var newForm = prototype.replace(/\\\$\\\$name\\\$\\\$/g, collectionHolder.children().length);

    // Display the form in the page in an li, before the \"Add a tag\" link li
    var \$newFormLi = \$('<li></li>').append(newForm);
    \$newLinkLi.before(\$newFormLi);

\t // add a delete link to the new form
    addTagFormDeleteLink(\$newFormLi);
}

function addTagFormDeleteLink(\$tagFormLi) {
    var \$removeFormA = \$('<a href=\"#\">Eliminar</a>');
    \$tagFormLi.append(\$removeFormA);

    \$removeFormA.on('click', function(e) {
        // prevent the link from creating a \"#\" on the URL
        e.preventDefault();

        // remove the li for the tag form
        \$tagFormLi.remove();
    });
}

\$(\".del_categoria\").click(function(event){
\t// prevent the link from creating a \"#\" on the URL
    event.preventDefault();\t
    var answer = confirm('¿Seguro que quieres eliminar la categoría?');
    if(answer){
\t\tvar index = \$(this).attr(\"id\");\t \t     
\t\t\$('#li'+index).remove()
    }   \t\t\t\t\t 
});\t\t

</script>\t
";
    }

    public function getTemplateName()
    {
        return "ClubBundle:Default:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  238 => 83,  235 => 82,  226 => 77,  223 => 76,  217 => 74,  214 => 73,  212 => 72,  209 => 71,  201 => 68,  195 => 66,  190 => 65,  188 => 64,  182 => 63,  175 => 62,  171 => 60,  165 => 57,  161 => 56,  155 => 53,  151 => 52,  145 => 49,  141 => 48,  135 => 45,  131 => 44,  125 => 41,  121 => 40,  115 => 37,  111 => 36,  107 => 34,  100 => 31,  96 => 30,  88 => 26,  83 => 23,  79 => 22,  74 => 19,  72 => 18,  68 => 17,  62 => 14,  56 => 13,  53 => 12,  48 => 8,  45 => 7,  38 => 4,  35 => 3,  29 => 2,);
    }
}
