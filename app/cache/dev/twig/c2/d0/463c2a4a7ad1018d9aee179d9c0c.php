<?php

/* ClubBundle:Default:widget/form.html.twig */
class __TwigTemplate_c2d0463c2a4a7ad1018d9aee179d9c0c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- Formulario general de un club  -->
<div class=\"box_ficha\">
\t";
        // line 3
        echo $this->env->getExtension('form')->renderErrors($this->getContext($context, "form"));
        echo "
\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
\t\t<tr>
\t\t\t<td><strong>";
        // line 6
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "nombre"));
        echo "</strong></td>
\t\t\t<td>";
        // line 7
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "nombre"), array("attr" => array("size" => "40")));
        echo "</td> 
\t\t</tr>
\t\t<tr>
\t\t\t<td><strong>";
        // line 10
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "imagen"));
        echo "</strong></td>
\t\t\t<td>";
        // line 11
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "imagen"));
        echo "</td>\t\t\t
\t\t</tr>\t\t
\t\t<tr>
\t\t\t<td><strong>";
        // line 14
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "direccion"));
        echo "</strong></td>
\t\t\t<td>";
        // line 15
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "direccion"));
        echo "</td>
\t\t</tr>
\t\t<tr>
\t\t\t<td><strong>";
        // line 18
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "ciudad"));
        echo "</strong></td>
\t\t\t<td>";
        // line 19
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "ciudad"));
        echo "</td>\t\t\t
\t\t</tr>
\t\t<tr>
\t\t\t<td><strong>";
        // line 22
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "telefono"));
        echo "</strong></td>
\t\t\t<td>";
        // line 23
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "telefono"));
        echo "</td>\t\t\t
\t\t</tr>\t\t
\t\t<tr>
\t\t\t<td><strong>";
        // line 26
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "email"));
        echo "</strong></td>
\t\t\t<td>";
        // line 27
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "email"));
        echo "</td>\t\t\t
\t\t</tr>\t\t\t
\t\t<tr>
\t\t\t<td><strong>";
        // line 30
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getContext($context, "form"), "activo"));
        echo "</strong></td>
\t\t\t<td>";
        // line 31
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "activo"));
        echo "</td>\t\t\t
\t\t</tr>
\t</table>\t
\t<ul class=\"categorias\" data-prototype=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "categorias"), "vars"), "prototype")));
        echo "\">\t
        ";
        // line 36
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "categorias"));
        foreach ($context['_seq'] as $context["_key"] => $context["categoria"]) {
            echo "        \t
            <li>";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "categorias"), "vars"), "prototype"), "categoria")));
            echo "</li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categoria'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 39
        echo "    </ul>        \t\t\t
</div>








";
    }

    public function getTemplateName()
    {
        return "ClubBundle:Default:widget/form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 39,  108 => 37,  101 => 36,  97 => 34,  91 => 31,  87 => 30,  81 => 27,  77 => 26,  71 => 23,  67 => 22,  61 => 19,  57 => 18,  51 => 15,  47 => 14,  41 => 11,  37 => 10,  31 => 7,  27 => 6,  21 => 3,  30 => 5,  26 => 3,  20 => 2,  17 => 1,);
    }
}
