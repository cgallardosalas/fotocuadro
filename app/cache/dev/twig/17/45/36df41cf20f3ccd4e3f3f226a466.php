<?php

/* PedidoBundle:Default:widget/pedido_productos.html.twig */
class __TwigTemplate_174536df41cf20f3ccd4e3f3f226a466 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"sub-title\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "categoria"), "club"), "nombre"), "html", null, true);
        echo " / ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "categoria"), "nombre"), "html", null, true);
        echo "</div>
";
        // line 2
        if (($this->getAttribute($this->getAttribute($this->getContext($context, "categoria"), "club"), "imagen") && ($this->getAttribute($this->getAttribute($this->getContext($context, "categoria"), "club"), "imagen") != ""))) {
            echo "\t\t\t\t\t\t
    \t<img alt=\"";
            // line 3
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "categoria"), "club"), "imagen"), "html", null, true);
            echo "\" class=\"foto\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(("fotos/club/" . $this->getAttribute($this->getAttribute($this->getContext($context, "categoria"), "club"), "imagen"))), "html", null, true);
            echo "\" width=\"60\" />  \t\t\t        \t\t\t        \t\t\t\t        \t                
";
        }
        // line 4
        echo "\t
<br /><br />
<div class=\"izquierda\">
\t";
        // line 7
        if (($this->getAttribute($this->getContext($context, "foto"), "archivo") && ($this->getAttribute($this->getContext($context, "foto"), "archivo") != ""))) {
            echo "\t\t\t\t\t\t
    \t<img alt=\"";
            // line 8
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "foto"), "nombre"), "html", null, true);
            echo "\" class=\"foto\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((("fotos/galeria/" . $this->getAttribute($this->getContext($context, "galeria"), "id")) . "/") . $this->getAttribute($this->getContext($context, "foto"), "archivo"))), "html", null, true);
            echo "\" width=\"300\" />  \t\t\t        \t\t\t        \t\t\t\t        \t                
\t";
        }
        // line 10
        echo "</div>
<div class=\"derecha\">
\t";
        // line 12
        if ((!(null === $this->getContext($context, "indice")))) {
            // line 13
            echo "\t\t";
            $context["accion"] = "pedido_guardar_producto";
            // line 14
            echo "\t";
        } else {
            // line 15
            echo "\t\t";
            $context["accion"] = "pedido_confirmar";
            echo "\t\t
\t";
        }
        // line 16
        echo "\t
\t<form action=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getContext($context, "accion")), "html", null, true);
        echo "\" method=\"post\" > 
\t\t<input type=\"hidden\" name=\"galeria_id\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "galeria"), "id"), "html", null, true);
        echo "\"> \t\t\t\t\t\t\t\t
\t\t<input type=\"hidden\" name=\"indice_foto\" value=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->getContext($context, "indice"), "html", null, true);
        echo "\">
\t\t<input type=\"hidden\" name=\"fotoId\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "foto"), "id"), "html", null, true);
        echo "\">
\t\t<table cellspacing=\"2\" cellpadding=\"2\" border=\"0\" width=\"80%\">
\t\t\t<thead>
\t\t        <tr>\t\t        \t
\t\t        \t<th>&nbsp;</th>            \t            \t            
\t\t            <th>Uds.</th>
\t\t            <th>PVP</th>
\t\t            <th>Total</th>\t\t            
\t\t            <th>&nbsp;</th>
\t\t        </tr>
\t\t\t  </thead>
\t\t    <tbody>\t\t\t    \t\t    \t\t  
\t\t    ";
        // line 32
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "tipos"));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["tipo"]) {
            echo "\t\t    \t\t\t    \t
\t\t    \t<tr>\t\t    \t\t\t\t    \t\t\t    \t\t\t\t    \t\t\t    \t\t\t
\t\t    \t\t<td><button type=\"button\" name=\"tipo_producto\">";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "tipo"), "nombre"), "html", null, true);
            echo "</button></td>
\t\t    \t\t<td><input type=\"text\" size=\"6\" maxlength=\"2\" name=\"tipo_cantidad[]\"></td>\t
\t\t    \t\t<td><input type=\"text\" size=\"6\" name=\"tipo_pvp[]\" readonly=\"readonly\" value=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "tipo"), "precio"), "html", null, true);
            echo "\"></td>
\t\t    \t\t<td><input type=\"text\" size=\"6\" name=\"tipo_importe[]\" readonly=\"readonly\"></td>
\t\t    \t\t<td><img alt=\"Borrar\" src=\"";
            // line 38
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/btn_borrar.gif"), "html", null, true);
            echo "\" /></td>
\t\t    \t</tr>\t\t    \t\t\t    \t\t\t    \t\t\t    \t\t\t    \t    
\t    \t";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 41
            echo "\t        \t<p>Ha ocurrido un error al preparar el pedido</p>
\t    \t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tipo'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 42
        echo "        \t\t       \t\t    \t  \t\t    \t\t\t     
\t\t    </tbody>
\t\t</table>
\t\t";
        // line 45
        if ((!(null === $this->getContext($context, "indice")))) {
            // line 46
            echo "\t\t\t<button type=\"submit\">Siguiente</button>
\t\t";
        } else {
            // line 48
            echo "\t\t\t<button type=\"submit\">Confirmar pedido</button>\t\t\t
\t\t";
        }
        // line 49
        echo "\t\t\t\t \t  \t\t\t\t\t   
\t</form>\t\t\t\t   \t
</div>


";
    }

    public function getTemplateName()
    {
        return "PedidoBundle:Default:widget/pedido_productos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 49,  143 => 48,  139 => 46,  137 => 45,  132 => 42,  125 => 41,  117 => 38,  112 => 36,  107 => 34,  99 => 32,  84 => 20,  80 => 19,  76 => 18,  72 => 17,  69 => 16,  63 => 15,  60 => 14,  57 => 13,  55 => 12,  51 => 10,  44 => 8,  40 => 7,  35 => 4,  28 => 3,  24 => 2,  17 => 1,);
    }
}
