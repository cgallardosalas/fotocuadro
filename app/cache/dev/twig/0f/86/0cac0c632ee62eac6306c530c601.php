<?php

/* PedidoBundle:Default:new.html.twig */
class __TwigTemplate_0f860cac0c632ee62eac6306c530c601 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'contenido' => array($this, 'block_contenido'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Pedidos";
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "        \t        \t        
";
    }

    // line 7
    public function block_contenido($context, array $blocks = array())
    {
        // line 8
        echo "\t<div class=\"contenido\">\t\t
\t\t<div class=\"title\">Pedidos</div>\t\t
    \t";
        // line 10
        if (($this->getContext($context, "paso") == 1)) {
            // line 11
            echo "    \t\t";
            $this->env->loadTemplate("PedidoBundle:Default:widget/pedido_clubes.html.twig")->display($context);
            // line 12
            echo "    \t";
        } elseif (($this->getContext($context, "paso") == 2)) {
            // line 13
            echo "    \t\t";
            $this->env->loadTemplate("PedidoBundle:Default:widget/pedido_categorias.html.twig")->display($context);
            // line 14
            echo "    \t";
        } elseif (($this->getContext($context, "paso") == 3)) {
            // line 15
            echo "    \t\t";
            $this->env->loadTemplate("PedidoBundle:Default:widget/pedido_galerias.html.twig")->display($context);
            // line 16
            echo "    \t";
        } elseif (($this->getContext($context, "paso") == 4)) {
            // line 17
            echo "    \t\t";
            $this->env->loadTemplate("PedidoBundle:Default:widget/pedido_fotos.html.twig")->display($context);
            // line 18
            echo "    \t";
        } elseif (($this->getContext($context, "paso") == 5)) {
            // line 19
            echo "    \t\t";
            $this->env->loadTemplate("PedidoBundle:Default:widget/pedido_productos.html.twig")->display($context);
            // line 20
            echo "    \t";
        }
        // line 21
        echo "\t\t\t\t
\t</div>\t\t\t\t\t\t\t
";
    }

    // line 24
    public function block_javascripts($context, array $blocks = array())
    {
        // line 25
        echo "\t";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
\t<script type=\"text/javascript\">
\t\tjQuery(document).ready(function() {
\t\t\t\$('.image-checkbox-container img').live('click', function(){
\t\t\t\talert(\"asasas\");
\t\t\t    if(!\$(this).prev('input[type=\"checkbox\"]').prop('checked')){
\t\t\t        \$(this).prev('input[type=\"checkbox\"]').prop('checked', true).attr('checked','checked');
\t\t\t        this.style.border = '4px solid #38A';
\t\t\t        this.style.margin = '0px';
\t\t\t    }else{
\t\t\t        \$(this).prev('input[type=\"checkbox\"]').prop('checked', false).removeAttr('checked');
\t\t\t        this.style.border = '0';
\t\t\t        this.style.margin = '4px';
\t\t\t    }
\t\t\t});​\t
\t\t});
\t</script>\t
";
    }

    public function getTemplateName()
    {
        return "PedidoBundle:Default:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 25,  90 => 24,  84 => 21,  81 => 20,  78 => 19,  75 => 18,  72 => 17,  69 => 16,  66 => 15,  63 => 14,  60 => 13,  57 => 12,  54 => 11,  52 => 10,  48 => 8,  45 => 7,  38 => 4,  35 => 3,  29 => 2,);
    }
}
