<?php

/* GaleriaBundle:Default:widget/listado_fotos.html.twig */
class __TwigTemplate_4f0ea3247daf27ddb4e8671545bc3d11 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"listado_fotos\">\t
\t<!--<div class=\"sub-title\">Fotos</div>-->\t
\t<button type=\"button\" onclick=\"location.href='";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("categoria_galerias", array("club" => $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "galeria"), "categoria"), "club"), "id"), "id" => $this->getAttribute($this->getAttribute($this->getContext($context, "galeria"), "categoria"), "id"))), "html", null, true);
        echo "';\" value=\"volver\" name=\"volver\">Volver</button>
\t    ";
        // line 4
        $context["i"] = 0;
        echo "\t\t    \t\t  
\t    ";
        // line 5
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "galeria"), "fotos"));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["foto"]) {
            echo "\t\t    \t
\t    \t";
            // line 6
            if (($this->getContext($context, "i") == 0)) {
                // line 7
                echo "\t    \t";
            }
            echo "\t\t    \t\t\t 
    \t\t<div class=\"caja_foto\">
\t    \t\t<a class=\"fotos\" href=\"";
            // line 9
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((("fotos/galeria/" . $this->getAttribute($this->getContext($context, "galeria"), "id")) . "/") . $this->getAttribute($this->getContext($context, "foto"), "archivo"))), "html", null, true);
            echo "\">
\t    \t\t";
            // line 11
            echo "\t\t\t\t";
            if (($this->getAttribute($this->getContext($context, "foto"), "archivo") && ($this->getAttribute($this->getContext($context, "foto"), "archivo") != ""))) {
                echo "\t\t\t\t\t\t
\t    \t\t\t\t<img alt=\"";
                // line 12
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "foto"), "nombre"), "html", null, true);
                echo "\" src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((("fotos/galeria/" . $this->getAttribute($this->getContext($context, "galeria"), "id")) . "/") . $this->getAttribute($this->getContext($context, "foto"), "archivo"))), "html", null, true);
                echo "\" width=\"200\" />  \t\t\t        \t\t\t        \t\t\t\t        \t                
\t\t\t\t";
            }
            // line 14
            echo "\t\t\t\t</a>
\t\t\t\t<br />
\t\t\t\t&nbsp;&nbsp;&nbsp;";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "foto"), "nombre"), "html", null, true);
            echo "
\t    \t\t<a title=\"Eliminar\" onclick=\"return confirm('¿Seguro que quieres eliminar la foto?');\" href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("foto_delete", array("id" => $this->getAttribute($this->getContext($context, "foto"), "id"))), "html", null, true);
            echo "\"><img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/delete.png"), "html", null, true);
            echo "\" alt=\"Eliminar\" width=\"20\" class=\"delete\" /></a>
    \t\t</div>\t
    \t";
            // line 19
            $context["i"] = ($this->getContext($context, "i") + 1);
            echo "   \t
\t    \t";
            // line 20
            if (($this->getContext($context, "i") == 4)) {
                // line 21
                echo "\t    \t\t";
                $context["i"] = 0;
                // line 22
                echo "\t    \t";
            }
            echo "\t\t    \t\t\t    \t\t\t    \t\t\t    \t      
    \t\t";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 24
            echo "        \t\t<p>No hay fotos para mostrar</p>
    \t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['foto'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 26
        echo "</div>
<div class=\"clear\"></div>";
    }

    public function getTemplateName()
    {
        return "GaleriaBundle:Default:widget/listado_fotos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 26,  92 => 24,  84 => 22,  81 => 21,  79 => 20,  75 => 19,  68 => 17,  64 => 16,  60 => 14,  53 => 12,  48 => 11,  44 => 9,  38 => 7,  36 => 6,  29 => 5,  25 => 4,  21 => 3,  17 => 1,);
    }
}
