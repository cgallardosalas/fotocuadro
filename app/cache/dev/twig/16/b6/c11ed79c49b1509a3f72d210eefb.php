<?php

/* ::cabecera.html.twig */
class __TwigTemplate_16b6c11ed79c49b1509a3f72d210eefb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"cabecera\">
\t<div id=\"menu_usuario\">
\t\t";
        // line 4
        echo "\t\t";
        $this->env->loadTemplate("::menu_usuario.html.twig")->display($context);
        echo "\t
\t</div>
    <div id=\"opciones\">
    \t";
        // line 8
        echo "    \t";
        $this->env->loadTemplate("::opciones.html.twig")->display($context);
        echo "    
    </div>  
</div>
";
    }

    public function getTemplateName()
    {
        return "::cabecera.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 8,  17 => 1,  119 => 38,  114 => 37,  109 => 26,  102 => 7,  99 => 6,  94 => 5,  87 => 40,  85 => 37,  80 => 34,  78 => 33,  75 => 32,  66 => 26,  60 => 23,  42 => 14,  33 => 9,  31 => 6,  27 => 5,  21 => 4,  238 => 83,  235 => 82,  226 => 77,  223 => 76,  217 => 74,  214 => 73,  212 => 72,  209 => 71,  201 => 68,  195 => 66,  190 => 65,  188 => 64,  182 => 63,  175 => 62,  171 => 60,  165 => 57,  161 => 56,  155 => 53,  151 => 52,  145 => 49,  141 => 48,  135 => 45,  131 => 44,  125 => 41,  121 => 40,  115 => 37,  111 => 36,  107 => 34,  100 => 31,  96 => 30,  88 => 26,  83 => 23,  79 => 22,  74 => 19,  72 => 18,  68 => 27,  62 => 24,  56 => 22,  53 => 21,  48 => 8,  45 => 15,  38 => 4,  35 => 3,  29 => 2,);
    }
}
