<?php

/* PedidoBundle:Default:widget/listado.html.twig */
class __TwigTemplate_e5ff64ecdb0d583b2aa74b2d9f70b4f9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"listado_pedidos\">\t
\t<div class=\"sub-title\">Gesti&oacute;n</div>\t\t
\t<table cellspacing=\"2\" cellpadding=\"2\" border=\"0\">\t\t
\t    <thead>
\t        <tr>\t\t        \t
\t        \t<th>ID Pedido</th>            \t            \t            
\t            <th>Fecha</th>\t\t            
\t            <th>Importe</th>
\t            <th>Usuario</th>
\t            <th>Estado</th>
\t        </tr>
\t    </thead>
\t    <tbody>\t\t\t       \t\t    \t  
\t    ";
        // line 14
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "pedidos"));
        foreach ($context['_seq'] as $context["_key"] => $context["pedido"]) {
            echo "\t\t    \t\t    \t\t\t    \t\t    
\t        <tr>
\t        \t";
            // line 17
            echo "\t\t\t\t<td><a href=\"#\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "pedido"), "id"), "html", null, true);
            echo "</a></td>\t        \t\t        \t\t\t        \t   
\t        \t<td>";
            // line 18
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "pedido"), "fecha"), "m/d/Y"), "html", null, true);
            echo "</td>
\t        \t<td>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "pedido"), "importe"), "html", null, true);
            echo " &euro;</td>
\t        \t<td>";
            // line 20
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getContext($context, "pedido"), "usuario"), "nombre") . " ") . $this->getAttribute($this->getAttribute($this->getContext($context, "pedido"), "usuario"), "apellidos")), "html", null, true);
            echo "</td>
\t        \t<td>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "estados"), $this->getAttribute($this->getContext($context, "pedido"), "estado"), array(), "array"), "html", null, true);
            echo "</td>\t        \t\t        \t\t\t        
\t        </tr>\t\t        
\t    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pedido'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 23
        echo "\t\t\t      
\t    </tbody>
\t</table>
\t";
        // line 27
        echo "\t";
        // line 28
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "PedidoBundle:Default:widget/listado.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 27,  65 => 23,  56 => 21,  52 => 20,  44 => 18,  39 => 17,  32 => 14,  17 => 1,  84 => 26,  81 => 25,  72 => 28,  67 => 16,  63 => 15,  60 => 14,  57 => 13,  53 => 11,  48 => 19,  45 => 7,  38 => 4,  35 => 3,  29 => 2,);
    }
}
