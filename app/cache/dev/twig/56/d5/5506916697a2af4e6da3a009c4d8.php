<?php

/* GaleriaBundle:Default:index.html.twig */
class __TwigTemplate_56d55506916697a2af4e6da3a009c4d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'contenido' => array($this, 'block_contenido'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Galerías";
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "        \t        \t        
";
    }

    // line 6
    public function block_contenido($context, array $blocks = array())
    {
        // line 7
        echo "\t<div class=\"contenido\">\t\t\t
\t\t<div class=\"title\">";
        // line 8
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getContext($context, "categoria"), "club"), "nombre") . " / ") . $this->getAttribute($this->getContext($context, "categoria"), "nombre")), "html", null, true);
        echo "</div>
\t\t<div class=\"centro\">
\t\t\t<form action=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("galeria_create"), "html", null, true);
        echo "\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo ">  \t\t\t
\t\t\t\t";
        // line 11
        echo $this->env->getExtension('form')->renderRow($this->getAttribute($this->getContext($context, "form"), "_token"));
        echo "
\t\t\t\t<strong><label for=\"nombre\">Crear galer&iacute;a</label></strong>
\t\t\t\t";
        // line 13
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "nombre"), array("attr" => array("size" => "30")));
        echo "
\t\t\t\t<input type=\"hidden\" id=\"galeria_categoria\" name=\"galeria[categoria]\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "categoria"), "id"), "html", null, true);
        echo "\">\t\t\t\t\t\t\t\t\t\t\t\t \t\t\t\t\t
\t\t\t\t<button type=\"submit\">Crear</button>
\t\t\t\t";
        // line 16
        if ($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "hasFlash", array(0 => "notice"), "method")) {
            // line 17
            echo "    \t\t\t\t<div class=\"flash-notice\">
        \t\t\t\t";
            // line 18
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flash", array(0 => "notice"), "method"), "html", null, true);
            echo "
    \t\t\t\t</div>
\t\t\t\t";
        }
        // line 20
        echo "\t\t\t\t\t \t  \t\t\t\t\t   
\t\t\t</form>
    \t\t";
        // line 23
        echo "    \t\t";
        $this->env->loadTemplate("GaleriaBundle:Default:widget/listado.html.twig")->display($context);
        // line 24
        echo "\t\t</div>
\t</div>
\t
\t<button type=\"button\" onclick=\"location.href='";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("club_edit", array("id" => $this->getAttribute($this->getAttribute($this->getContext($context, "categoria"), "club"), "id"))), "html", null, true);
        echo "';\" value=\"volver\" name=\"volver\">Volver</button>
\t<br /><br />
";
    }

    // line 31
    public function block_javascripts($context, array $blocks = array())
    {
        // line 32
        echo "\t";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "GaleriaBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 32,  106 => 31,  99 => 27,  94 => 24,  91 => 23,  87 => 20,  81 => 18,  78 => 17,  76 => 16,  71 => 14,  67 => 13,  62 => 11,  56 => 10,  51 => 8,  48 => 7,  45 => 6,  38 => 4,  35 => 3,  29 => 2,);
    }
}
