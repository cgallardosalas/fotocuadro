<?php

/* PedidoBundle:Default:widget/pedido_fotos.html.twig */
class __TwigTemplate_658f8c5b6032fed504f5a246138f1ef6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"clear\"></div>
<div id=\"listado_fotos\">
\t<div class=\"sub-title\">";
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "galeria"), "categoria"), "club"), "nombre"), "html", null, true);
        echo " / ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "galeria"), "categoria"), "nombre"), "html", null, true);
        echo "</div>
\t";
        // line 4
        if (($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "galeria"), "categoria"), "club"), "imagen") && ($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "galeria"), "categoria"), "club"), "imagen") != ""))) {
            echo "\t\t\t\t\t\t
    \t<img alt=\"";
            // line 5
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "galeria"), "categoria"), "club"), "imagen"), "html", null, true);
            echo "\" class=\"foto\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(("fotos/club/" . $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "galeria"), "categoria"), "club"), "imagen"))), "html", null, true);
            echo "\" width=\"60\" />  \t\t\t        \t\t\t        \t\t\t\t        \t                
\t";
        }
        // line 6
        echo "\t
\t<form action=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pedido_new_seleccion"), "html", null, true);
        echo "\" method=\"post\" > 
\t\t<input type=\"hidden\" name=\"galeria_id\" value=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "galeria"), "id"), "html", null, true);
        echo "\"> 
\t\t    ";
        // line 9
        $context["i"] = 0;
        echo "\t\t    
\t\t    <ul class=\"image-checkbox-container checklist\">\t\t  
\t\t    ";
        // line 11
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "galeria"), "fotos"));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["foto"]) {
            echo "\t\t    \t
\t\t    \t";
            // line 12
            if (($this->getContext($context, "i") == 0)) {
                echo "    \t\t\t\t    \t\t\t    \t\t
\t\t    \t";
            }
            // line 13
            echo "\t
\t\t    \t\t<li>\t\t
\t        \t\t\t";
            // line 15
            if (($this->getAttribute($this->getContext($context, "foto"), "archivo") && ($this->getAttribute($this->getContext($context, "foto"), "archivo") != ""))) {
                echo "\t
\t        \t\t\t \t<a class=\"fotos\" href=\"";
                // line 16
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((("fotos/galeria/" . $this->getAttribute($this->getContext($context, "galeria"), "id")) . "/") . $this->getAttribute($this->getContext($context, "foto"), "archivo"))), "html", null, true);
                echo "\">\t\t\t\t\t
\t    \t\t\t\t\t<img alt=\"";
                // line 17
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "foto"), "nombre"), "html", null, true);
                echo "\" class=\"foto\" src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(((("fotos/galeria/" . $this->getAttribute($this->getContext($context, "galeria"), "id")) . "/") . $this->getAttribute($this->getContext($context, "foto"), "archivo"))), "html", null, true);
                echo "\" width=\"200\" />
\t    \t\t\t\t\t</a>  \t\t\t        \t\t\t        \t\t\t\t        \t                
\t\t\t\t\t\t";
            }
            // line 20
            echo "\t        \t\t\t<input type=\"checkbox\" name=\"fotos_pedido[]\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "foto"), "id"), "html", null, true);
            echo "\" />
\t        \t\t\t<a class=\"checkbox-select\" href=\"#\">Seleccionar</a>
\t\t\t\t\t\t<a class=\"checkbox-deselect\" href=\"#\">Cancelar</a>
\t\t\t\t\t</li>
\t\t    \t";
            // line 24
            $context["i"] = ($this->getContext($context, "i") + 1);
            echo "   \t
\t\t    \t";
            // line 25
            if (($this->getContext($context, "i") == 4)) {
                // line 26
                echo "\t\t    \t\t";
                $context["i"] = 0;
                // line 27
                echo "\t\t    \t";
            }
            echo "\t\t    \t\t\t    \t\t\t    \t\t\t    \t      
\t    \t\t";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 29
            echo "\t        \t\t<p>No hay fotos para mostrar</p>
\t    \t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['foto'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 31
        echo "\t\t    </ul>\t
\t";
        // line 32
        if ($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "hasFlash", array(0 => "notice"), "method")) {
            // line 33
            echo "    \t<div class=\"flash-notice\">
        \t";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flash", array(0 => "notice"), "method"), "html", null, true);
            echo "
    \t</div>
\t";
        }
        // line 36
        echo "\t\t\t\t\t\t \t  \t\t\t\t\t   
\t</form>\t
</div>
<div class=\"clear\"></div>
<button type=\"submit\" class=\"boton_big\">Continuar</button>";
    }

    public function getTemplateName()
    {
        return "PedidoBundle:Default:widget/pedido_fotos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 36,  126 => 34,  123 => 33,  121 => 32,  118 => 31,  111 => 29,  103 => 27,  100 => 26,  98 => 25,  94 => 24,  86 => 20,  78 => 17,  74 => 16,  70 => 15,  66 => 13,  61 => 12,  54 => 11,  49 => 9,  45 => 8,  41 => 7,  38 => 6,  31 => 5,  27 => 4,  21 => 3,  17 => 1,);
    }
}
