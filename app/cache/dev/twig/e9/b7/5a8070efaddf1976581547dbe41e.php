<?php

/* GaleriaBundle:Default:widget/listado.html.twig */
class __TwigTemplate_e9b75a8070efaddf1976581547dbe41e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"listado_galerias\">\t
\t<!--  <div class=\"sub-title\">Galer&iacute;as</div>-->
    ";
        // line 3
        $context["i"] = 0;
        echo "\t\t    \t\t  
    ";
        // line 4
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "categoria"), "galerias"));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["galeria"]) {
            echo "\t\t    \t
    \t";
            // line 5
            if (($this->getContext($context, "i") == 0)) {
                echo "    \t\t\t\t    \t\t\t    \t\t
    \t";
            }
            // line 6
            echo "\t\t\t
    \t\t<div class=\"caja_galeria\">
\t    \t\t<a href=\"";
            // line 8
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("galeria_show", array("id" => $this->getAttribute($this->getContext($context, "galeria"), "id"))), "html", null, true);
            echo "\"><img alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "galeria"), "nombre"), "html", null, true);
            echo "\" width=\"200\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "galeria"), "nombre"), "html", null, true);
            echo "\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(("fotos/galeria/" . $this->getAttribute($this->getContext($context, "galeria"), "portada"))), "html", null, true);
            echo "\"><br /></a>
    \t\t";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "galeria"), "nombre"), "html", null, true);
            echo "
    \t\t<a title=\"Eliminar\" onclick=\"return confirm('¿Seguro que quieres eliminar la galería?');\" href=\"";
            // line 10
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("galeria_delete", array("id" => $this->getAttribute($this->getContext($context, "galeria"), "id"))), "html", null, true);
            echo "\">Eliminar</a> \t    \t\t
    \t\t</div>\t   
\t    \t";
            // line 12
            $context["i"] = ($this->getContext($context, "i") + 1);
            echo "   \t
    \t";
            // line 13
            if (($this->getContext($context, "i") == 4)) {
                // line 14
                echo "    \t\t";
                $context["i"] = 0;
                // line 15
                echo "    \t";
            }
            echo "\t\t    \t\t\t    \t\t\t    \t\t\t    \t      
    \t\t";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 17
            echo "        \t\t<p>No hay galerías para mostrar</p>
    \t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['galeria'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 19
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "GaleriaBundle:Default:widget/listado.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 19,  77 => 17,  69 => 15,  66 => 14,  64 => 13,  60 => 12,  55 => 10,  41 => 8,  37 => 6,  32 => 5,  25 => 4,  21 => 3,  17 => 1,  103 => 29,  100 => 28,  94 => 24,  91 => 23,  87 => 20,  81 => 18,  78 => 17,  76 => 16,  71 => 14,  67 => 13,  62 => 11,  56 => 10,  51 => 9,  48 => 7,  45 => 6,  38 => 4,  35 => 3,  29 => 2,);
    }
}
