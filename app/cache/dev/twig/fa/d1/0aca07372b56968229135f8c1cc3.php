<?php

/* ClubBundle:Default:widget/listado.html.twig */
class __TwigTemplate_fad10aca07372b56968229135f8c1cc3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"listado_clubes\">\t
\t<div class=\"sub-title\">Gesti&oacute;n</div>\t
\t<table cellspacing=\"2\" cellpadding=\"2\" border=\"0\">
\t\t    <thead>
\t\t        <tr>\t\t        \t
\t\t        \t<th>&nbsp;</th>            \t            \t            
\t\t            <th>Equipo</th>\t\t            
\t\t        </tr>
\t\t    </thead>
\t\t    <tbody>\t\t\t       \t\t    \t  
\t\t    ";
        // line 11
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "lista"));
        foreach ($context['_seq'] as $context["_key"] => $context["club"]) {
            echo "\t\t    \t\t    \t\t\t    \t\t    
\t\t        <tr>\t\t        \t   
\t\t        \t<td>
\t\t        \t";
            // line 14
            if (($this->getAttribute($this->getContext($context, "club"), "imagen") && ($this->getAttribute($this->getContext($context, "club"), "imagen") != ""))) {
                echo "\t\t\t\t\t\t
    \t\t\t\t\t<img alt=\"";
                // line 15
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "club"), "imagen"), "html", null, true);
                echo "\" class=\"foto\" src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(("fotos/club/" . $this->getAttribute($this->getContext($context, "club"), "imagen"))), "html", null, true);
                echo "\" width=\"40\" />  \t\t\t        \t\t\t        \t\t\t\t        \t                
\t\t\t\t\t";
            }
            // line 17
            echo "\t\t\t\t\t</td>
\t\t        \t<td><a href=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("club_edit", array("id" => $this->getAttribute($this->getContext($context, "club"), "id"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "club"), "nombre"), "html", null, true);
            echo "</a></td>
\t\t        \t";
            // line 19
            echo "\t\t        \t
\t\t        </tr>\t\t        
\t\t    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['club'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 21
        echo "\t\t\t      
\t\t    </tbody>
\t\t</table>
\t\t";
        // line 25
        echo "\t\t";
        // line 26
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "ClubBundle:Default:widget/listado.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 25,  65 => 21,  51 => 18,  41 => 15,  37 => 14,  17 => 1,  92 => 29,  89 => 28,  79 => 22,  76 => 21,  74 => 20,  72 => 26,  67 => 16,  63 => 15,  60 => 14,  57 => 19,  53 => 11,  48 => 17,  45 => 7,  38 => 4,  35 => 3,  29 => 11,);
    }
}
