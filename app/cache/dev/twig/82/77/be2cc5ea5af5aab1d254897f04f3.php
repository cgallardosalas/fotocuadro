<?php

/* PedidoBundle:Default:widget/pedido_categorias.html.twig */
class __TwigTemplate_8277be2cc5ea5af5aab1d254897f04f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"tabla_categorias\">\t
\t<span class=\"sub-title\">";
        // line 2
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "club"), "nombre"), "html", null, true);
        echo "</span>
\t";
        // line 3
        if (($this->getAttribute($this->getContext($context, "club"), "imagen") && ($this->getAttribute($this->getContext($context, "club"), "imagen") != ""))) {
            echo "\t\t\t\t\t\t
    \t<img alt=\"";
            // line 4
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "club"), "imagen"), "html", null, true);
            echo "\" class=\"foto\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(("fotos/club/" . $this->getAttribute($this->getContext($context, "club"), "imagen"))), "html", null, true);
            echo "\" width=\"60\" />  \t\t\t        \t\t\t        \t\t\t\t        \t                
\t";
        }
        // line 5
        echo "\t
\t<br /><br />
\t<div class=\"sub-sub-title\">Categor&iacute;as</div>\t
\t    ";
        // line 8
        $context["i"] = 0;
        echo "\t\t    \t\t  
\t    ";
        // line 9
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "club"), "categorias"));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["categoria"]) {
            echo "\t\t    \t
\t    \t";
            // line 10
            if (($this->getContext($context, "i") == 0)) {
                echo "    \t\t\t\t    \t\t\t    \t\t
\t    \t";
            }
            // line 11
            echo "\t   \t\t\t    \t\t
\t    \t\t<button type=\"button\" class=\"boton_big\" onclick=\"location.href='";
            // line 12
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pedido_new_galerias", array("categoria" => $this->getAttribute($this->getContext($context, "categoria"), "id"))), "html", null, true);
            echo "';\" name=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "categoria"), "nombre"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "categoria"), "nombre"), "html", null, true);
            echo "</button>
\t    \t";
            // line 13
            $context["i"] = ($this->getContext($context, "i") + 1);
            echo "   \t
\t    \t";
            // line 14
            if (($this->getContext($context, "i") == 2)) {
                // line 15
                echo "\t    \t\t";
                $context["i"] = 0;
                // line 16
                echo "\t    \t";
            }
            echo "\t\t    \t\t\t    \t\t\t    \t\t\t    \t      
    \t\t";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 18
            echo "        \t\t<p>No hay categor&iacute;s para mostrar</p>
    \t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['categoria'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 20
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "PedidoBundle:Default:widget/pedido_categorias.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 20,  84 => 18,  76 => 16,  73 => 15,  71 => 14,  67 => 13,  59 => 12,  56 => 11,  51 => 10,  44 => 9,  40 => 8,  35 => 5,  28 => 4,  24 => 3,  20 => 2,  17 => 1,);
    }
}
