<?php

/* UsuarioBundle:Default:login.html.twig */
class __TwigTemplate_5e56df1085284b9fc4d4c11db401a996 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"es\">
    <head>
        <meta charset=\"UTF-8\" />
        <title>Foto Cuadro | Login</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 9
        echo "        <link rel=\"shortcut icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />        
    </head>    
    <body>
    <div id=\"contenedor\"> <!-- contenedor -->
    \t<header></header>    \t
    \t<!-- cuerpo del html -->
    \t<article id=\"wrap\">    \t\t
    \t\t<div class=\"contenido\">
\t\t\t\t";
        // line 17
        if ($this->getContext($context, "error")) {
            // line 18
            echo "\t\t\t\t\t<!-- MENSAJE DE AVISO -->
\t\t\t\t\t<div class=\"aviso warning\">\t\t\t
\t\t\t\t\t<img src=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/elplaneadorbackend/images/span_warning.gif"), "html", null, true);
            echo "\" alt=\"Warning\" />
\t\t\t\t\t";
            // line 21
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute($this->getContext($context, "error"), "message")), "html", null, true);
            echo "\t\t
\t\t\t\t\t</div>\t\t
\t\t\t\t\t<!-- /MENSAJE DE AVISO -->
\t\t\t\t";
        }
        // line 24
        echo "\t\t
\t\t\t\t<div class=\"box_login\">\t
\t\t\t\t\t<img alt=\"FotografiArte\" src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("images/fotografiarte_logo.jpg"), "html", null, true);
        echo "\" />
\t\t\t\t\t<form action=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("login_check_usuario"), "html", null, true);
        echo "\" method=\"post\">
\t\t\t\t\t\t<!-- 
\t\t\t\t\t\t<div class=\"title\">Acceso</div>
\t\t\t\t\t\t<label class=\"text\">Escribe tu usuario y contrase&ntilde;a para acceder a la administraci&oacute;n</label>
\t\t\t\t\t\t -->
\t\t\t\t\t\t<div class=\"clear\"></div>\t\t\t\t\t
\t\t\t\t\t    <label for=\"username\">Usuario</label>
\t\t\t\t\t    <input id=\"username\" type=\"text\" name=\"_username\" value=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->getContext($context, "last_username"), "html", null, true);
        echo "\" />
\t\t\t\t\t    <div class=\"clear\"></div>\t\t    \t\t   
\t\t\t\t\t    <label for=\"password\">Contrase&ntilde;a</label>
\t\t\t\t\t    <input id=\"password\" type=\"password\" name=\"_password\" />
\t\t\t\t\t    <div class=\"clear\"></div>
\t\t\t\t\t     
\t\t\t\t\t    <input type=\"submit\" name=\"login\" value=\"Acceder\" class=\"boton_acceder boton dcha\" />
\t\t\t\t\t</form>      \t
\t\t\t\t</div>
\t\t\t</div>
    \t</article>
\t\t<!-- /cuerpo del html -->               \t        \t            \t\t\t    \t            
    </div> <!-- /contenedor -->
    </body>    
</html>";
    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 7
        echo "        \t<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\" />        \t        
        ";
    }

    public function getTemplateName()
    {
        return "UsuarioBundle:Default:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 7,  93 => 6,  74 => 34,  64 => 27,  60 => 26,  56 => 24,  49 => 21,  45 => 20,  41 => 18,  39 => 17,  27 => 9,  25 => 6,  18 => 1,);
    }
}
