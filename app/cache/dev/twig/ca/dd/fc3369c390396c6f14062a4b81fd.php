<?php

/* PedidoBundle:Default:widget/pedido_clubes.html.twig */
class __TwigTemplate_caddfc3369c390396c6f14062a4b81fd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"tabla_clubes\">\t
\t<div class=\"sub-title\">Seleccione el club</div>\t\t
\t<table cellspacing=\"2\" cellpadding=\"2\" border=\"0\">
\t\t<thead>
\t        <tr>\t\t        \t
\t        \t<th>&nbsp;</th>            \t            \t            
\t            <th>&nbsp;</th>
\t            <th>&nbsp;</th>
\t            <th>&nbsp;</th>\t\t            
\t        </tr>
\t\t  </thead>
\t    <tbody>\t
\t    ";
        // line 13
        $context["i"] = 0;
        echo "\t\t    \t\t  
\t    ";
        // line 14
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "lista"));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["club"]) {
            echo "\t\t    \t
\t    \t";
            // line 15
            if (($this->getContext($context, "i") == 0)) {
                // line 16
                echo "\t    \t\t<tr>\t\t    \t\t\t\t    \t\t\t    \t\t
\t    \t";
            }
            // line 17
            echo "\t\t    \t\t\t 
\t    \t<td>\t    \t\t
\t    \t\t<div class=\"caja_club\">\t    \t\t
\t    \t\t<a title=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "club"), "nombre"), "html", null, true);
            echo "\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pedido_new_categorias", array("idClub" => $this->getAttribute($this->getContext($context, "club"), "id"))), "html", null, true);
            echo "\">\t    \t\t
\t    \t\t";
            // line 22
            echo "\t\t\t\t";
            if (($this->getAttribute($this->getContext($context, "club"), "imagen") && ($this->getAttribute($this->getContext($context, "club"), "imagen") != ""))) {
                echo "\t\t\t\t\t\t
    \t\t\t\t<img alt=\"";
                // line 23
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "club"), "imagen"), "html", null, true);
                echo "\" class=\"foto\" src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(("fotos/club/" . $this->getAttribute($this->getContext($context, "club"), "imagen"))), "html", null, true);
                echo "\" width=\"200\" />  \t\t\t        \t\t\t        \t\t\t\t        \t                
\t\t\t\t";
            }
            // line 25
            echo "\t\t\t\t<br />";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "club"), "nombre"), "html", null, true);
            echo "
\t\t\t\t</a> \t    \t\t
\t    \t\t</div>\t
\t    \t</td>
\t    \t";
            // line 29
            $context["i"] = ($this->getContext($context, "i") + 1);
            echo "   \t
\t    \t";
            // line 30
            if (($this->getContext($context, "i") == 4)) {
                // line 31
                echo "\t    \t\t";
                $context["i"] = 0;
                // line 32
                echo "\t    \t\t</tr>
\t    \t";
            }
            // line 33
            echo "\t\t    \t\t\t    \t\t\t    \t\t\t    \t      
    \t\t";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 35
            echo "        \t\t<p>No hay clubes para mostrar</p>
    \t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['club'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 36
        echo "        \t\t       \t\t    \t  \t\t    \t\t\t     
\t    </tbody>
\t</table>\t
</div>";
    }

    public function getTemplateName()
    {
        return "PedidoBundle:Default:widget/pedido_clubes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 36,  98 => 35,  92 => 33,  88 => 32,  85 => 31,  83 => 30,  79 => 29,  71 => 25,  64 => 23,  59 => 22,  53 => 20,  44 => 16,  42 => 15,  31 => 13,  17 => 1,  93 => 25,  90 => 24,  84 => 21,  81 => 20,  78 => 19,  75 => 18,  72 => 17,  69 => 16,  66 => 15,  63 => 14,  60 => 13,  57 => 12,  54 => 11,  52 => 10,  48 => 17,  45 => 7,  38 => 4,  35 => 14,  29 => 2,);
    }
}
