<?php

/* PedidoBundle:Default:index.html.twig */
class __TwigTemplate_5f70dc8b860d0eb03038507bf8bd6c26 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'contenido' => array($this, 'block_contenido'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Pedidos";
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "        \t        \t        
";
    }

    // line 7
    public function block_contenido($context, array $blocks = array())
    {
        // line 8
        echo "\t<div class=\"contenido\">\t\t
\t\t<div class=\"title\">Pedidos</div>
\t\t<div class=\"izquierda\">\t
\t\t\t";
        // line 11
        if ($this->getContext($context, "pedidos")) {
            echo "\t\t\t    \t\t\t
    \t\t\t";
            // line 13
            echo "    \t\t\t";
            $this->env->loadTemplate("PedidoBundle:Default:widget/listado.html.twig")->display($context);
            // line 14
            echo "    \t\t";
        } else {
            // line 15
            echo "    \t\t\t<p>No hay pedidos para mostrar</p>\t    \t\t\t
    \t\t";
        }
        // line 16
        echo "    \t\t
\t\t</div>
\t\t<div class=\"derecha\">
\t\t\t<button type=\"button\" class=\"boton_big\" onclick=\"location.href='";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pedido_new_clubes"), "html", null, true);
        echo "';\" value=\"realizar_pedido\" name=\"realizar_pedido\">Realizar pedido</button>\t\t\t   \t
\t\t</div>
\t\t
\t</div>\t\t\t\t\t\t\t
";
    }

    // line 25
    public function block_javascripts($context, array $blocks = array())
    {
        // line 26
        echo "\t";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "\t\t
";
    }

    public function getTemplateName()
    {
        return "PedidoBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 26,  81 => 25,  72 => 19,  67 => 16,  63 => 15,  60 => 14,  57 => 13,  53 => 11,  48 => 8,  45 => 7,  38 => 4,  35 => 3,  29 => 2,);
    }
}
