<?php

/* ClubBundle:Default:new.html.twig */
class __TwigTemplate_efd6c847410cc0405bef096add22bcbc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"sub-title\">Crear club</div>
<form action=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("club_create"), "html", null, true);
        echo "\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo ">  \t\t\t
\t\t";
        // line 3
        echo $this->env->getExtension('form')->renderRow($this->getAttribute($this->getContext($context, "form"), "_token"));
        echo "
\t\t";
        // line 5
        echo "\t\t";
        $this->env->loadTemplate("ClubBundle:Default:widget/form.html.twig")->display($context);
        echo "\t\t\t
\t\t<button type=\"submit\">Aceptar</button>\t\t\t\t\t \t  \t\t\t\t\t   
</form>




";
    }

    public function getTemplateName()
    {
        return "ClubBundle:Default:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 5,  26 => 3,  20 => 2,  17 => 1,);
    }
}
