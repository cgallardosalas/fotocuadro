<?php

/* UsuarioBundle:Default:panel.html.twig */
class __TwigTemplate_fd2513ed7fc77a32e7255f524a8784f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'contenido' => array($this, 'block_contenido'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Panel de control";
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "        \t        \t        
";
    }

    // line 7
    public function block_contenido($context, array $blocks = array())
    {
        // line 8
        echo "\t<div class=\"contenido\">\t
\t\t<div class=\"title\">Panel de control</div>
\t\t<div class=\"izquierda\">\t
\t\t\t";
        // line 12
        echo "\t\t\t<p>Listado de pedidos</p>    \t\t
\t\t</div>
\t\t<div class=\"derecha\">
\t\t\t";
        // line 15
        if ($this->getContext($context, "form")) {
            // line 16
            echo "\t\t\t\t";
            // line 17
            echo "\t    \t\t";
            $this->env->loadTemplate("ClubBundle:Default:new.html.twig")->display($context);
            // line 18
            echo "    \t\t";
        }
        echo "\t    \t\t
\t\t</div>
\t</div>\t\t\t\t\t\t\t
";
    }

    // line 23
    public function block_javascripts($context, array $blocks = array())
    {
        // line 24
        echo "\t";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "\t\t
";
    }

    public function getTemplateName()
    {
        return "UsuarioBundle:Default:panel.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 24,  74 => 23,  65 => 18,  62 => 17,  60 => 16,  58 => 15,  53 => 12,  48 => 8,  45 => 7,  38 => 4,  35 => 3,  29 => 2,);
    }
}
