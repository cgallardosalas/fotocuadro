<?php

/* PedidoBundle:Default:widget/pedido_galerias.html.twig */
class __TwigTemplate_61536b98ca9a967710a2d5ff793a5d86 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"tabla_galerias\">\t
\t<div class=\"sub-title\">";
        // line 2
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "categoria"), "club"), "nombre"), "html", null, true);
        echo " / ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "categoria"), "nombre"), "html", null, true);
        echo "</div>
\t";
        // line 3
        if (($this->getAttribute($this->getAttribute($this->getContext($context, "categoria"), "club"), "imagen") && ($this->getAttribute($this->getAttribute($this->getContext($context, "categoria"), "club"), "imagen") != ""))) {
            echo "\t\t\t\t\t\t
    \t<img alt=\"";
            // line 4
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "categoria"), "club"), "imagen"), "html", null, true);
            echo "\" class=\"foto\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(("fotos/club/" . $this->getAttribute($this->getAttribute($this->getContext($context, "categoria"), "club"), "imagen"))), "html", null, true);
            echo "\" width=\"60\" />    \t  \t\t\t        \t\t\t        \t\t\t\t        \t                
\t";
        }
        // line 6
        echo "\t<br />\t\t
    ";
        // line 7
        $context["i"] = 0;
        echo "\t\t    \t\t  
    ";
        // line 8
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "categoria"), "galerias"));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["galeria"]) {
            echo "\t\t    \t
    \t";
            // line 9
            if (($this->getContext($context, "i") == 0)) {
                echo "    \t\t\t\t    \t\t\t    \t\t
    \t";
            }
            // line 10
            echo "\t
    \t\t<div class=\"caja_galeria\">\t    \t\t
\t    \t\t<a title=\"";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "galeria"), "nombre"), "html", null, true);
            echo "\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("pedido_new_fotos", array("galeria" => $this->getAttribute($this->getContext($context, "galeria"), "id"))), "html", null, true);
            echo "\">\t    \t\t\t    \t
    \t\t<img alt=\"";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "galeria"), "nombre"), "html", null, true);
            echo "\" width=\"200\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "galeria"), "nombre"), "html", null, true);
            echo "\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl(("fotos/galeria/" . $this->getAttribute($this->getContext($context, "galeria"), "portada"))), "html", null, true);
            echo "\">
\t\t\t<br />&nbsp;&nbsp;&nbsp;";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "galeria"), "nombre"), "html", null, true);
            echo "
\t\t\t<br />&nbsp;&nbsp;&nbsp;";
            // line 15
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "galeria"), "fecha"), "d/m/Y"), "html", null, true);
            echo "
\t\t\t\t</a>    \t\t
    \t\t</div>\t
\t    \t";
            // line 18
            $context["i"] = ($this->getContext($context, "i") + 1);
            echo "   \t
    \t";
            // line 19
            if (($this->getContext($context, "i") == 4)) {
                // line 20
                echo "    \t\t";
                $context["i"] = 0;
                // line 21
                echo "    \t";
            }
            echo "\t\t    \t\t\t    \t\t\t    \t\t\t    \t      
    \t\t";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 23
            echo "        \t\t<p>No hay galer&iacute;as para mostrar</p>
    \t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['galeria'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 24
        echo "       
</div>";
    }

    public function getTemplateName()
    {
        return "PedidoBundle:Default:widget/pedido_galerias.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 24,  101 => 23,  93 => 21,  90 => 20,  88 => 19,  84 => 18,  78 => 15,  74 => 14,  66 => 13,  60 => 12,  56 => 10,  51 => 9,  44 => 8,  40 => 7,  37 => 6,  30 => 4,  26 => 3,  20 => 2,  17 => 1,);
    }
}
