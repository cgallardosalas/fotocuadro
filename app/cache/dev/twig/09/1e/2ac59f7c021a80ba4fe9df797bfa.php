<?php

/* ::base.html.twig */
class __TwigTemplate_091e2ac59f7c021a80ba4fe9df797bfa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'contenido' => array($this, 'block_contenido'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"es\">
    <head>
        <meta charset=\"UTF-8\" />
        <title>Foto Cuadro | ";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 10
        echo "        <link rel=\"shortcut icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />        
    </head>    
    <body>
    <div id=\"contenedor\"> <!-- contenedor -->
    \t<header>\t\t
    \t\t";
        // line 15
        echo "    
\t\t\t";
        // line 16
        $this->env->loadTemplate("::cabecera.html.twig")->display($context);
        echo "    \t\t\t    \t\t    \t\t    \t
    \t</header>    \t
    \t<!-- cuerpo del html -->
    \t<article id=\"wrap\">
    \t\t<div class=\"menu\">
\t    \t\t";
        // line 22
        echo "\t\t\t\t";
        $this->env->loadTemplate("::logo.html.twig")->display($context);
        // line 23
        echo "\t\t\t\t";
        echo "    
\t\t\t\t";
        // line 24
        $this->env->loadTemplate("::menu.html.twig")->display($context);
        // line 25
        echo "    \t\t</div>
    \t\t<div class=\"contenido\">
    \t\t\t";
        // line 27
        $this->displayBlock('contenido', $context, $blocks);
        // line 28
        echo "    \t\t</div>
    \t</article>
\t\t<!-- /cuerpo del html -->
        <!-- Pie -->
        <footer>\t\t
\t\t\t";
        // line 33
        echo "    
\t\t\t";
        // line 34
        $this->env->loadTemplate("::footer.html.twig")->display($context);
        // line 35
        echo "\t\t</footer>\t\t      
        <!-- /Pie -->
                \t        \t            \t\t\t    \t     
        ";
        // line 38
        $this->displayBlock('javascripts', $context, $blocks);
        // line 43
        echo "    </div> <!-- /contenedor -->
    </body>    
</html>
";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 7
        echo "        \t<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\" />  
        \t<link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/colorbox.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\" />
        ";
    }

    // line 27
    public function block_contenido($context, array $blocks = array())
    {
    }

    // line 38
    public function block_javascripts($context, array $blocks = array())
    {
        echo "  
        \t<script type=\"text/javascript\" src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-1.7.2.min.js"), "html", null, true);
        echo "\"></script>
        \t<script type=\"text/javascript\" src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.colorbox-min.js"), "html", null, true);
        echo "\"></script>  
        \t<script type=\"text/javascript\" src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/functions.js"), "html", null, true);
        echo "\"></script>        \t  \t      \t        \t
        ";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 41,  127 => 40,  123 => 39,  118 => 38,  113 => 27,  107 => 8,  102 => 7,  99 => 6,  94 => 5,  87 => 43,  85 => 38,  80 => 35,  78 => 34,  75 => 33,  68 => 28,  66 => 27,  62 => 25,  60 => 24,  56 => 23,  53 => 22,  45 => 16,  42 => 15,  33 => 10,  31 => 6,  27 => 5,  21 => 1,);
    }
}
