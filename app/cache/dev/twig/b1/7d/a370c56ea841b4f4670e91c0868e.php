<?php

/* GaleriaBundle:Default:fotos.html.twig */
class __TwigTemplate_b17da370c56ea841b4f4670e91c0868e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("::base.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'contenido' => array($this, 'block_contenido'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Fotos";
    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "        \t        \t        
";
    }

    // line 7
    public function block_contenido($context, array $blocks = array())
    {
        // line 8
        echo "\t<div class=\"contenido\">\t\t
\t\t<div class=\"title\">";
        // line 9
        echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "galeria"), "categoria"), "club"), "nombre") . " / ") . $this->getAttribute($this->getAttribute($this->getContext($context, "galeria"), "categoria"), "nombre")), "html", null, true);
        echo "</div>
\t\t<div class=\"med-title\">";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "galeria"), "nombre"), "html", null, true);
        echo "</div>
\t\t<div class=\"centro\">\t
\t\t\t<form action=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("foto_create"), "html", null, true);
        echo "\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo ">  \t\t\t
\t\t\t\t";
        // line 13
        echo $this->env->getExtension('form')->renderRow($this->getAttribute($this->getContext($context, "form"), "_token"));
        echo "
\t\t\t\t<strong><label for=\"nombre\">Crear foto</label></strong>
\t\t\t\t";
        // line 15
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "archivo"));
        echo "
\t\t\t\t<input type=\"hidden\" id=\"foto_galeria\" name=\"foto[galeria]\" value=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "galeria"), "id"), "html", null, true);
        echo "\">\t\t\t\t\t\t\t\t\t\t\t\t \t\t\t\t\t
\t\t\t\t<button type=\"submit\">Crear</button>
\t\t\t\t";
        // line 18
        if ($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "hasFlash", array(0 => "notice"), "method")) {
            // line 19
            echo "    \t\t\t\t<div class=\"flash-notice\">
        \t\t\t\t";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flash", array(0 => "notice"), "method"), "html", null, true);
            echo "
    \t\t\t\t</div>
\t\t\t\t";
        }
        // line 22
        echo "\t\t\t\t\t \t  \t\t\t\t\t 
\t\t\t</form>\t\t
    \t\t";
        // line 25
        echo "    \t\t";
        $this->env->loadTemplate("GaleriaBundle:Default:widget/listado_fotos.html.twig")->display($context);
        // line 26
        echo "\t\t</div>
\t</div>\t\t\t\t\t\t\t
";
    }

    public function getTemplateName()
    {
        return "GaleriaBundle:Default:fotos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 26,  94 => 25,  90 => 22,  84 => 20,  81 => 19,  79 => 18,  74 => 16,  70 => 15,  65 => 13,  59 => 12,  54 => 10,  50 => 9,  47 => 8,  44 => 7,  37 => 4,  34 => 3,  28 => 2,);
    }
}
