<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appdevUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appdevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = urldecode($pathinfo);

        // _wdt
        if (preg_match('#^/_wdt/(?P<token>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::toolbarAction',)), array('_route' => '_wdt'));
        }

        if (0 === strpos($pathinfo, '/_profiler')) {
            // _profiler_search
            if ($pathinfo === '/_profiler/search') {
                return array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchAction',  '_route' => '_profiler_search',);
            }

            // _profiler_purge
            if ($pathinfo === '/_profiler/purge') {
                return array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::purgeAction',  '_route' => '_profiler_purge',);
            }

            // _profiler_import
            if ($pathinfo === '/_profiler/import') {
                return array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::importAction',  '_route' => '_profiler_import',);
            }

            // _profiler_export
            if (0 === strpos($pathinfo, '/_profiler/export') && preg_match('#^/_profiler/export/(?P<token>[^/\\.]+?)\\.txt$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::exportAction',)), array('_route' => '_profiler_export'));
            }

            // _profiler_search_results
            if (preg_match('#^/_profiler/(?P<token>[^/]+?)/search/results$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::searchResultsAction',)), array('_route' => '_profiler_search_results'));
            }

            // _profiler
            if (preg_match('#^/_profiler/(?P<token>[^/]+?)$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController::panelAction',)), array('_route' => '_profiler'));
            }

        }

        if (0 === strpos($pathinfo, '/_configurator')) {
            // _configurator_home
            if (rtrim($pathinfo, '/') === '/_configurator') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', '_configurator_home');
                }
                return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
            }

            // _configurator_step
            if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]+?)$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',)), array('_route' => '_configurator_step'));
            }

            // _configurator_final
            if ($pathinfo === '/_configurator/final') {
                return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
            }

        }

        // pedido_edit
        if (0 === strpos($pathinfo, '/pedido/edit') && preg_match('#^/pedido/edit/(?P<id>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::editAction',)), array('_route' => 'pedido_edit'));
        }

        // pedido_new_clubes
        if (rtrim($pathinfo, '/') === '/pedido/new/clubes') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pedido_new_clubes');
            }
            return array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::newClubesAction',  '_route' => 'pedido_new_clubes',);
        }

        // pedido_new_categorias
        if (0 === strpos($pathinfo, '/pedido/new/clubes') && preg_match('#^/pedido/new/clubes/(?P<idClub>[^/]+?)/categorias/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pedido_new_categorias');
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::newCategoriasAction',)), array('_route' => 'pedido_new_categorias'));
        }

        // pedido_new_galerias
        if (0 === strpos($pathinfo, '/pedido/new/clubes/categorias') && preg_match('#^/pedido/new/clubes/categorias/(?P<categoria>[^/]+?)/galerias/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pedido_new_galerias');
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::newGaleriasAction',)), array('_route' => 'pedido_new_galerias'));
        }

        // pedido_new_fotos
        if (0 === strpos($pathinfo, '/pedido/new/clubes/categorias/galerias') && preg_match('#^/pedido/new/clubes/categorias/galerias/(?P<galeria>[^/]+?)/fotos/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pedido_new_fotos');
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::newFotosAction',)), array('_route' => 'pedido_new_fotos'));
        }

        // pedido_new_seleccion
        if (rtrim($pathinfo, '/') === '/pedido/new/clubes/categorias/galerias/fotos') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_pedido_new_seleccion;
            }
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pedido_new_seleccion');
            }
            return array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::seleccionFotosAction',  '_route' => 'pedido_new_seleccion',);
        }
        not_pedido_new_seleccion:

        // pedido_guardar_producto
        if (rtrim($pathinfo, '/') === '/pedido/guardar/producto') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_pedido_guardar_producto;
            }
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pedido_guardar_producto');
            }
            return array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::guardarProductoAction',  '_route' => 'pedido_guardar_producto',);
        }
        not_pedido_guardar_producto:

        // pedido_confirmar
        if (rtrim($pathinfo, '/') === '/pedido/confirmar') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pedido_confirmar');
            }
            return array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::confirmarAction',  '_route' => 'pedido_confirmar',);
        }

        // pedido_crear
        if (rtrim($pathinfo, '/') === '/pedido/crear') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_pedido_crear;
            }
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'pedido_crear');
            }
            return array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::crearAction',  '_route' => 'pedido_crear',);
        }
        not_pedido_crear:

        // galeria_create
        if (rtrim($pathinfo, '/') === '/galeria/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_galeria_create;
            }
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'galeria_create');
            }
            return array (  '_controller' => 'FotoCuadro\\GaleriaBundle\\Controller\\DefaultController::createAction',  '_route' => 'galeria_create',);
        }
        not_galeria_create:

        // galeria_show
        if (0 === strpos($pathinfo, '/galeria/show') && preg_match('#^/galeria/show/(?P<id>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FotoCuadro\\GaleriaBundle\\Controller\\DefaultController::showAction',)), array('_route' => 'galeria_show'));
        }

        // galeria_delete
        if (0 === strpos($pathinfo, '/galeria/delete') && preg_match('#^/galeria/delete/(?P<id>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FotoCuadro\\GaleriaBundle\\Controller\\DefaultController::deleteAction',)), array('_route' => 'galeria_delete'));
        }

        // foto_delete
        if (0 === strpos($pathinfo, '/galeria/foto/delete') && preg_match('#^/galeria/foto/delete/(?P<id>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FotoCuadro\\GaleriaBundle\\Controller\\DefaultController::deleteFotoAction',)), array('_route' => 'foto_delete'));
        }

        // foto_create
        if (rtrim($pathinfo, '/') === '/galeria/foto/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_foto_create;
            }
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'foto_create');
            }
            return array (  '_controller' => 'FotoCuadro\\GaleriaBundle\\Controller\\DefaultController::createFotoAction',  '_route' => 'foto_create',);
        }
        not_foto_create:

        // club_create
        if (rtrim($pathinfo, '/') === '/club/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_club_create;
            }
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'club_create');
            }
            return array (  '_controller' => 'FotoCuadro\\ClubBundle\\Controller\\DefaultController::createAction',  '_route' => 'club_create',);
        }
        not_club_create:

        // club_edit
        if (0 === strpos($pathinfo, '/club/edit') && preg_match('#^/club/edit/(?P<id>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FotoCuadro\\ClubBundle\\Controller\\DefaultController::editAction',)), array('_route' => 'club_edit'));
        }

        // club_update
        if (0 === strpos($pathinfo, '/club/update') && preg_match('#^/club/update/(?P<id>[^/]+?)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_club_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FotoCuadro\\ClubBundle\\Controller\\DefaultController::updateAction',)), array('_route' => 'club_update'));
        }
        not_club_update:

        // club_delete
        if (0 === strpos($pathinfo, '/club/delete') && preg_match('#^/club/delete/(?P<id>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FotoCuadro\\ClubBundle\\Controller\\DefaultController::deleteAction',)), array('_route' => 'club_delete'));
        }

        // categoria_galerias
        if (0 === strpos($pathinfo, '/club') && preg_match('#^/club/(?P<club>[^/]+?)/categoria/(?P<id>[^/]+?)/galerias$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FotoCuadro\\ClubBundle\\Controller\\DefaultController::galeriasAction',)), array('_route' => 'categoria_galerias'));
        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }
            return array (  '_controller' => 'FotoCuadro\\UsuarioBundle\\Controller\\DefaultController::panelAction',  '_route' => 'homepage',);
        }

        // login_usuario
        if ($pathinfo === '/login') {
            return array (  '_controller' => 'FotoCuadro\\UsuarioBundle\\Controller\\DefaultController::loginAction',  '_route' => 'login_usuario',);
        }

        // login_check_usuario
        if ($pathinfo === '/login_check') {
            return array('_route' => 'login_check_usuario');
        }

        // logout_usuario
        if ($pathinfo === '/logout') {
            return array('_route' => 'logout_usuario');
        }

        if (0 === strpos($pathinfo, '/usuarios')) {
            // usuario_create
            if (rtrim($pathinfo, '/') === '/usuarios/usuario/create') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_usuario_create;
                }
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'usuario_create');
                }
                return array (  '_controller' => 'FotoCuadro\\UsuarioBundle\\Controller\\DefaultController::createAction',  '_route' => 'usuario_create',);
            }
            not_usuario_create:

            // usuario_edit
            if (0 === strpos($pathinfo, '/usuarios/usuario/edit') && preg_match('#^/usuarios/usuario/edit/(?P<id>[^/]+?)$#s', $pathinfo, $matches)) {
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FotoCuadro\\UsuarioBundle\\Controller\\DefaultController::editAction',)), array('_route' => 'usuario_edit'));
            }

            // usuario_update
            if (0 === strpos($pathinfo, '/usuarios/usuario/update') && preg_match('#^/usuarios/usuario/update/(?P<id>[^/]+?)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_usuario_update;
                }
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FotoCuadro\\UsuarioBundle\\Controller\\DefaultController::updateAction',)), array('_route' => 'usuario_update'));
            }
            not_usuario_update:

        }

        // menu_panel
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'menu_panel');
            }
            return array (  '_controller' => 'FotoCuadro\\UsuarioBundle\\Controller\\DefaultController::panelAction',  '_route' => 'menu_panel',);
        }

        // menu_pedido
        if (rtrim($pathinfo, '/') === '/pedidos') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'menu_pedido');
            }
            return array (  '_controller' => 'FotoCuadro\\PedidoBundle\\Controller\\DefaultController::indexAction',  '_route' => 'menu_pedido',);
        }

        // menu_club
        if (rtrim($pathinfo, '/') === '/clubes') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'menu_club');
            }
            return array (  '_controller' => 'FotoCuadro\\ClubBundle\\Controller\\DefaultController::indexAction',  '_route' => 'menu_club',);
        }

        // menu_usuario
        if (rtrim($pathinfo, '/') === '/usuarios') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'menu_usuario');
            }
            return array (  '_controller' => 'FotoCuadro\\UsuarioBundle\\Controller\\DefaultController::indexAction',  '_route' => 'menu_usuario',);
        }

        // menu_configuracion
        if (rtrim($pathinfo, '/') === '/configuracion') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'menu_configuracion');
            }
            return array (  '_controller' => 'FotoCuadro\\UsuarioBundle\\Controller\\DefaultController::configuracionAction',  '_route' => 'menu_configuracion',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
